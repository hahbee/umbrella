ifneq ($(MAKECMDGOALS),clean)
ifeq ($(SYSROOT),)
$(error I need the sysroot to your Rust build)
endif
endif

SYSROOT := $(abspath $(SYSROOT))

RUSTC ?= $(shell readlink -f $(SYSROOT)/bin/rustc)
RUST_PNACL_TRANS ?= $(shell readlink -f $(SYSROOT)/bin/rust-pnacl-trans)

NACL_SDK  ?= $(shell readlink -f ~/workspace/tools/nacl-sdk/pepper_canary)

ifneq ($(MAKECMDGOALS),clean)
ifeq  ($(NACL_SDK),)
$(error I need the directory to your Pepper SDK!)
endif
endif

# deps
RUST_PPAPI   ?= $(shell readlink -f engine/deps/ppapi)

USE_DEBUG ?= 0
RUSTFLAGS += -C cross-path=$(NACL_SDK) -C nacl-flavor=pnacl --target=le32-unknown-nacl -L $(RUST_HTTP)/build --sysroot=$(shell readlink -f $(SYSROOT))
TOOLCHAIN ?= $(NACL_SDK)/toolchain/linux_pnacl

STAGING_DIR ?= $(abspath build)
TARGET_DIR ?= $(abspath www/priv/static)

$(STAGING_DIR):
	mkdir -p $@
$(TARGET_DIR):
	mkdir -p $@

ifeq ($(USE_DEBUG),0)

RUSTFLAGS += -O --cfg ndebug

else

RUSTFLAGS += --debuginfo=2 -Z no-opt

endif

rwildcard = $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

all: $(TARGET_DIR)/hahbee.nexe $(TARGET_DIR)/hahbee.pexe

.DEFAULT_GOAL:= all

clean:
	$(MAKE) clean -C $(RUST_PPAPI)                  \
		RUSTC="$(RUSTC)"                        \
		RUST_PNACL_TRANS="$(RUST_PNACL_TRANS)"  \
		SYSROOT="$(SYSROOT)"                    \
		NACL_SDK="$(NACL_SDK)"                  \
		BUILD_DIR="$(STAGING_DIR)"
	touch Makefile

$(STAGING_DIR)/hahbee.stamp: engine/src/main.rs           \
		       	     $(STAGING_DIR)               \
		       	     Makefile                     \
	               	     $(call rwildcard,engine,*rs) \
		       	     $(RUSTC)                     \
		       	     engine/deps/ppapi.stamp
	$(RUSTC) $(RUSTFLAGS) $< --out-dir $(STAGING_DIR) -L $(STAGING_DIR) --emit=link,bc -C stable-pexe
	touch $@

$(STAGING_DIR)/hahbee.bc: $(STAGING_DIR)/hahbee.stamp
$(STAGING_DIR)/hahbee.pexe: $(STAGING_DIR)/hahbee.stamp

$(TARGET_DIR)/hahbee.nexe: $(STAGING_DIR)/hahbee.bc
	$(RUST_PNACL_TRANS) -o $@ $< --cross-path=$(NACL_SDK)

$(TARGET_DIR)/hahbee.pexe: $(STAGING_DIR)/hahbee.pexe
	$(NACL_SDK)/toolchain/linux_pnacl/bin/pnacl-finalize --compress $< -o $@


engine/deps/ppapi.stamp: $(RUST_PPAPI)/Makefile         \
		  Makefile                              \
		  $(call rwildcard,$(RUST_PPAPI),*rs *c *h) \
		  $(RUSTC) $(RUST_PNACL_TRANS) | $(STAGING_DIR) $(TARGET_DIR)
	$(MAKE) -C $(RUST_PPAPI)                        \
		RUSTC="$(RUSTC)"                        \
		RUST_PNACL_TRANS="$(RUST_PNACL_TRANS)"  \
		SYSROOT="$(SYSROOT)"                    \
		NACL_SDK="$(NACL_SDK)"                  \
		BUILD_DIR="$(STAGING_DIR)"
