use infra;

use std::option::{Option, Some, None};
use std::{ops, collections};
use std::default::Default;
use std::clone::Clone;
use ppapi::Ticks;

pub trait Animated<ExtraP, ExtraR> {
    fn animate(&mut self, frame: &infra::Frame, extra: ExtraP) -> ExtraR;
}

pub struct Animatible<T> {
    puppet: T,
    start_frame: Option<infra::FrameId>,
    puppeteer: Option<Box<Animator<T>>>,
}
impl<T> Animatible<T> {
    pub fn new(puppet: T) -> Animatible<T> {
        Animatible {
            puppet: puppet,
            start_frame: None,
            puppeteer: None,
        }
    }

    pub fn reset(&mut self) {
        self.start_frame = None;
    }
    pub fn stop(&mut self) -> Option<Box<Animator<T>>> {
        self.puppeteer.take()
    }

    pub fn resume(&mut self, puppeteer: Box<Animator<T>>) {
        self.puppeteer = Some(puppeteer);
    } 
}
impl<T: Default> Default for Animatible<T> {
    fn default() -> Animatible<T> {
        Animatible {
            puppet: Default::default(),
            start_frame: Default::default(),
            puppeteer: None,
        }
    }
}
impl<T: Clone> Clone for Animatible<T> {
    fn clone(&self) -> Animatible<T> {
        Animatible::new(self.puppet.clone())
    }
}
impl<T> ops::Deref<T> for Animatible<T> {
    fn deref<'a>(&'a self) -> &'a T {
        &self.puppet
    }
}
pub enum ActionForNextFrame<T> {
    MarchOnAction(T),
    ResetAction(T),
    PausedAction(T),
    StopAfterAction(T),
}
pub trait Animator<T> {
    fn animate(&mut self,
               last: T,
               frame_id: infra::FrameId,
               delta: Ticks) -> ActionForNextFrame<T>;
}
impl<T> Animated<(), bool> for Animatible<T> {
    fn animate(&mut self, frame: &infra::Frame, _: ()) -> bool {
        use std::mem::{replace, uninitialized};
        use std::ptr::write;
        let puppeteer = self.puppeteer.take();
        match puppeteer {
            Some(mut puppeteer) => {
                let frame_id = if self.start_frame.is_none() {
                    self.start_frame = Some(frame.id.clone());
                    infra::FrameId(0)
                } else {
                    frame.id - self.start_frame.unwrap()
                };
                let action = puppeteer.animate(replace(&mut self.puppet, unsafe { uninitialized() }),
                                               frame_id,
                                               frame.delta);
                let (puppet, animated) = match action {
                    MarchOnAction(puppet) => {
                        self.puppeteer = Some(puppeteer);
                        (puppet, true)
                    }
                    ResetAction(puppet) => {
                        self.puppeteer = Some(puppeteer);
                        self.start_frame.take();
                        (puppet, true)
                    }
                    PausedAction(puppet) => {
                        self.puppeteer = Some(puppeteer);
                        self.start_frame.mutate(|id| id + infra::FrameId(1) );
                        (puppet, false)
                    }
                    StopAfterAction(puppet) => (puppet, true),
                };
                unsafe {
                    write(&mut self.puppet, puppet)
                };

                animated
            }
            None => false,
        }
    }
}

/// Animation by frame.
pub struct StopMotion<T> {
    poses: Vec<T>,
    pub paused: bool,
    pub looped: bool,
}

impl<T> StopMotion<T> {
    pub fn with_initial(initial_frame_count: Option<u64>, initial_pose: T) -> StopMotion<T> {
        let mut sm = StopMotion {
            poses: Vec::with_capacity(match initial_frame_count {
                    None => 0,
                    Some(count) => count as uint,
                } + 1),
            paused: false,
            looped: false,
        };
        sm.push_pose(initial_pose);
        sm
    }
    pub fn set_looped(&mut self, looped: bool) {
        self.looped = looped;
    }
    pub fn is_looped(&self) -> bool { self.looped }
    

    fn push_pose(&mut self, value: T) -> uint {
        self.poses.push(value);
        self.poses.len()
    }
}
impl<T> collections::Collection for StopMotion<T> {
    fn len(&self) -> uint { self.poses.len() }
}

impl<T: Clone> Animator<T> for StopMotion<T> {
    fn animate(&mut self,
               last: T,
               frame_id: infra::FrameId,
               _delta: Ticks) -> ActionForNextFrame<T> {
        if self.paused {
            PausedAction(last)
        } else if self.looped && self.poses.len() >= *frame_id {
            ResetAction(self.poses.as_slice()[*frame_id % self.poses.len()].clone())
        } else if self.poses.len() >= *frame_id {
            StopAfterAction(last)
        } else {
            MarchOnAction(self.poses.as_slice()[*frame_id].clone())
        }
    }
}
