use std::rc::Rc;
use std::str::MaybeOwned;
use ppapi::Context3d;
use ppapi::gles::{MaxVertexAttribs, MaxVertexUniformVectors, MaxVaryingVectors,
                  MaxCombinedTextureImageUnits, MaxVertexImageUnits,
                  MaxTextureImageUnits, MaxFragmentUniformVectors,
                  MaxCubeMapTextureSize, MaxRenderBufferSize, MaxTextureSize,
                  MaxColorAttachments,
                  Vendor, Renderer, Version, Extensions};

//#[deriving(Hash, Encodable, Decodable)]
pub struct Capabilities {
    pub vendor: MaybeOwned<'static>,
    pub renderer: MaybeOwned<'static>,
    pub version: MaybeOwned<'static>,
    pub extensions: Vec<MaybeOwned<'static>>,
    pub max_vertex_attribs: uint,
    pub max_vertex_uniform_vectors: uint,
    pub max_varying_vectors: uint,
    pub max_combined_texture_image_units: uint,
    pub max_vertex_image_units: uint,
    pub max_texture_image_units: uint,
    pub max_fragment_uniform_vectors: uint,
    pub max_cube_map_texture_size: uint,
    pub max_render_buffer_size: uint,
    pub max_texture_size: uint,
    pub max_color_attachments: uint,
}

pub type DevCaps = Rc<Capabilities>;

impl Capabilities {
    pub fn probe(ctxt: &Context3d) -> Rc<Capabilities> {
        Rc::new(Capabilities {
            vendor: ctxt.get(Vendor),
            renderer: ctxt.get(Renderer),
            version: ctxt.get(Version),
            extensions: ctxt.get(Extensions),
            
            max_vertex_attribs: ctxt.get(MaxVertexAttribs) as uint,
            max_vertex_uniform_vectors: ctxt.get(MaxVertexUniformVectors) as uint,
            max_varying_vectors: ctxt.get(MaxVaryingVectors) as uint,
            max_combined_texture_image_units: ctxt.get(MaxCombinedTextureImageUnits) as uint,
            max_vertex_image_units: ctxt.get(MaxVertexImageUnits) as uint,
            max_texture_image_units: ctxt.get(MaxTextureImageUnits) as uint,
            max_fragment_uniform_vectors: ctxt.get(MaxFragmentUniformVectors) as uint,
            max_cube_map_texture_size: ctxt.get(MaxCubeMapTextureSize) as uint,
            max_render_buffer_size: ctxt.get(MaxRenderBufferSize) as uint,
            max_texture_size: ctxt.get(MaxTextureSize) as uint,
            max_color_attachments: ctxt.get(MaxColorAttachments) as uint,
        })
    }
}
