use ppapi::gles::{TextureBuffer, BoundTexBuffer};
use ppapi::gles::types;
use ppapi::gles::traits::GenBuffer;
use ppapi::Context3d;
use ImageMap = ppapi::imagedata::Map;

use infra;
use infra::{InternalCache, DirtyCount};

use std::default::Default;

use super::shader::Program;
use super::{Sampler2dKind, SamplerCubeKind, Sampler2dValue, SamplerKind};

#[deriving(Clone)]
pub struct Material {
    pub tex_units: Vec<TextureUnit>,
}
impl Material {
    pub fn new() -> Material {
        Material {
            tex_units: Vec::new(),
        }
    }
    pub fn new_with_tex_units(units: Vec<TextureUnit>) -> Material {
        Material {
            tex_units: units,
        }
    }

    pub fn update_binding_values(&self, program: &mut Program) {
        use super::shader::{SamplerUniformRole};
        for (i, unit) in self.tex_units.iter().enumerate() {
            match unit.kind {
                Sampler2dKind =>
                    program.maybe_set(&SamplerUniformRole(unit.kind, i),
                                      Sampler2dValue(i as i32)),
                SamplerCubeKind =>
                    program.maybe_set(&SamplerUniformRole(unit.kind, i),
                                      Sampler2dValue(i as i32)),
            }
        }
    }

    pub fn activate(&self, ctxt: &mut Context3d) {
        for (i, unit) in self.tex_units.iter().enumerate() {
            unit.bind_and_activate(ctxt, i as u32);
        }
    }
}

#[deriving(Clone)]
pub struct TextureUnit {
    buffer: infra::InternalCache<uint, TextureBuffer>,
    dirty:  infra::DirtyCount,
    usage:  types::Enum,
    kind:   SamplerKind,
    src:    TextureUnitSource,
}

impl TextureUnit {
    pub fn new_from_tex_buf(tex: TextureBuffer) -> TextureUnit {
        use ppapi::gles::consts::TEXTURE_2D;
        let dirty: DirtyCount = Default::default();
        TextureUnit {
            dirty:  dirty.clone(),
            buffer: InternalCache::new_fresh(&dirty, tex.clone()),
            usage:  TEXTURE_2D,
            kind:   Sampler2dKind,
            src:    TextureBufferSource(tex),
        }
    }
    // assumes TEXTURE_2D usage.
    pub fn new_from_image_data(map: ImageMap) -> TextureUnit {
        use ppapi::gles::consts::TEXTURE_2D;
        use ppapi::imagedata::RGBA;
        assert!(map.desc.format == RGBA);
        TextureUnit {
            dirty:  Default::default(),
            buffer: InternalCache::new_stale(),
            usage:  TEXTURE_2D,
            kind:   Sampler2dKind,
            src:    ImageDataSource(map),
        }
    }
    /*pub fn format(&self) -> gles::TexFormat {
        match self.src {
        }
    }*/
    pub fn set_src(&mut self, src: TextureUnitSource) -> TextureUnitSource {
        use std::mem::replace;
        self.dirty.bump();
        replace(&mut self.src, src)
    }
    pub fn uses_pre_mult_alpha(&self) -> bool {
        match self.src {
            ImageDataSource(_) => true,
            _ => false,
        }
    }
    pub fn bind(&self, ctxt: &mut Context3d) -> BoundTexBuffer {
        let mut bound: Option<BoundTexBuffer> = None;
        let tbo = self.buffer.get(&self.dirty,
                        |last| {
                            match self.src {
                                TextureBufferSource(ref buf) => {
                                    return buf.clone();
                                }
                                _ => {}
                            }
                            let tbo = match last {
                                Some(tbo) => tbo,
                                None => GenBuffer::gen_single(ctxt),
                            };
                            bound = Some(tbo.bind(ctxt, self.usage));
                            tbo
                        });
        match bound {
            Some(b) => b,
            None => tbo.bind(ctxt, self.usage),
        }
    }
    pub fn bind_and_activate(&self,
                             ctxt: &mut Context3d,
                             slot: types::Enum) {
        let _ = self.bind(ctxt);
        ctxt.activate_tex_slot(slot);
    }
}

#[deriving(Clone)]
pub enum TextureUnitSource {
    TextureBufferSource(TextureBuffer),
    ImageDataSource(ImageMap),
}
