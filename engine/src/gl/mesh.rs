use super::shader::{VertexLocus};
use super::{Type, SingleFloatScalar, TypedMutRef, TypedImmRef};
use super::{Vec2Type, Vec3Type};
use super::traits::Typed;
use std::{cmp, iter};
use std::mem::size_of;
use std::cell::Cell;
use std::default::Default;

use ppapi::Context3d;
use ppapi::gles::{VertexBuffer, IndexBuffer, GeometryMode, BoundVertBuffer, BoundIdxBuffer};
use ppapi::gles::traits::{GenBuffer, BindableBuffer};

use infra::DirtyCount;
use collections::TreeMap;

#[deriving(PartialEq, Hash, Eq, Ord, PartialOrd, Show)]
pub struct Handle(u32);

#[deriving(Clone)]
pub enum IndicesBuffer {
    U16Indices(Vec<u16>)
}
impl Collection for IndicesBuffer {
    fn len(&self) -> uint {
        match self {
            &U16Indices(ref b) => b.len(),
        }
    }
}

#[deriving(Clone)]
pub struct Indices {
    buf:  IndicesBuffer,
    subs: Vec<SubMesh>,
}
impl Indices {
    
    pub fn new_simple(len: uint) -> Indices {
        Indices {
            buf: U16Indices(FromIterator::from_iter(iter::range(0u16, len as u16))),
            subs: vec!(SubMesh {
                slice_start: 0,
                slice_len: len,
                mode: Default::default(),
            }),
        }
    }
}

pub struct Mesh {
    ref_count:      Cell<uint>,
    // the number of modifications since we last updated the buffers.
    vbo_mod_count:  DirtyCount,
    ibo_mod_count:  DirtyCount,

    vbo: Cell<Option<VertexBuffer>>,
    ibo: Cell<Option<IndexBuffer>>,

    vertices: Vertices,
    indices:  Indices,
}
//pub type Mesh_ = RefCell<Mesh__>;
//pub type Mesh  = Rc<Mesh_>;

impl Mesh {
    pub fn new(verts: Vertices, indices: Indices) -> Mesh {
        Mesh {
            ref_count:     Cell::new(0),
            vbo_mod_count: Default::default(),
            ibo_mod_count: Default::default(),

            vbo:       Cell::new(Default::default()),
            ibo:       Cell::new(Default::default()),

            vertices:  verts,
            indices:   indices,
        }
    }
    pub fn dirty(&self) -> bool {
        self.is_vbo_dirty() || self.is_ibo_dirty()
    }

    pub fn is_vbo_dirty(&self) -> bool {
        self.vbo.get().is_none() || self.vbo_mod_count.dirty()
    }
    fn clean_vbo(&self, ctxt: &mut Context3d) -> BoundVertBuffer {
        use super::traits::Type;
        use super::{BoolType, I32Type, F32Type,
                    Vec2Type, Vec3Type,Vec4Type,
                    MatType,
                    SamplerType, ArrayType};
        use super::{BooleanScalar, SignedIntegerScalar, SingleFloatScalar};
        use ppapi::gles::{BufferSome, StaticBufferUsage, UByteType, FloatType};
        let vbo = self.vbo.get().unwrap_or_else(|| GenBuffer::gen_single(ctxt) );
        let bound = vbo.bind(ctxt);
        bound.buffer_vertex_data(ctxt, BufferSome(&self.vertices.buf), StaticBufferUsage);

        // FIXME: use Result.
        fn integer_fail() {
            fail!("can't use integer types in attributes")
        }
        fn sampler_fail() {
            fail!("can't use sampler types in attributes")
        }

        let mut offset = 0;
        for (i, attr) in self.vertices.attrs.iter().enumerate() {
            match attr.expose_type() {
                &BoolType => bound.vertex_attribute(ctxt, i, 1, UByteType, true, 0, Some(offset)),
                &I32Type => integer_fail(),
                &F32Type => bound.vertex_attribute(ctxt, i, 1, FloatType, false, 0, Some(offset)),

                &Vec2Type(BooleanScalar) =>
                    bound.vertex_attribute(ctxt, i, 2, UByteType, true, 0, Some(offset)),
                &Vec2Type(SignedIntegerScalar) => integer_fail(),
                &Vec2Type(SingleFloatScalar) =>
                    bound.vertex_attribute(ctxt, i, 2, FloatType, false, 0, Some(offset)),

                &Vec3Type(BooleanScalar) =>
                    bound.vertex_attribute(ctxt, i, 3, UByteType, true, 0, Some(offset)),
                &Vec3Type(SignedIntegerScalar) => integer_fail(),
                &Vec3Type(SingleFloatScalar) =>
                    bound.vertex_attribute(ctxt, i, 3, FloatType, false, 0, Some(offset)),

                &Vec4Type(BooleanScalar) =>
                    bound.vertex_attribute(ctxt, i, 4, UByteType, true, 0, Some(offset)),
                &Vec4Type(SignedIntegerScalar) => integer_fail(),
                &Vec4Type(SingleFloatScalar) =>
                    bound.vertex_attribute(ctxt, i, 4, FloatType, false, 0, Some(offset)),

                &MatType(size) =>
                    bound.vertex_attribute(ctxt, i, size.len(), FloatType, false, 0, Some(offset)),

                &SamplerType(_) => sampler_fail(),

                &ArrayType(_, _) => unimplemented!(),
            }
            offset += attr.byte_size();
        }
        self.vbo_mod_count.reset();
        bound
    }
    fn clean_and_bind_vbo(&self, ctxt: &mut Context3d) -> BoundVertBuffer {
        if self.is_vbo_dirty() {
            self.clean_vbo(ctxt)
        } else {
            self.vbo.get().unwrap().bind(ctxt)
        }
    }

    pub fn is_ibo_dirty(&self) -> bool {
        self.ibo.get().is_none() || self.ibo_mod_count.dirty()
    }
    fn clean_ibo(&self, ctxt: &mut Context3d) -> BoundIdxBuffer {
        use ppapi::gles::{BufferSome, StaticBufferUsage};
        let ibo = self.ibo.get().unwrap_or_else(|| GenBuffer::gen_single(ctxt) );
        let bound = ibo.bind(ctxt);
        match self.indices.buf {
            U16Indices(ref b) => bound.buffer_index_data(ctxt,
                                                         BufferSome(b),
                                                         StaticBufferUsage),
        }        
        self.ibo.set(Some(ibo));
        self.ibo_mod_count.reset();
        bound
    }
    fn clean_and_bind_ibo(&self, ctxt: &mut Context3d) -> BoundIdxBuffer {
        if self.is_ibo_dirty() {
            self.clean_ibo(ctxt)
        } else {
            self.ibo.get().unwrap().bind(ctxt)
        }
    }

    fn clean_and_bind_buffers(&self, ctxt: &mut Context3d) -> (BoundVertBuffer, BoundIdxBuffer) {
        (self.clean_and_bind_vbo(ctxt), self.clean_and_bind_ibo(ctxt))
    }

    pub fn replace_vertex_buffer(&mut self, buffer: Vertices) {
        use std::mem::replace;
        self.vbo_mod_count.bump();
        replace(&mut self.vertices, buffer);
    }
    pub fn modify_vertices<U>(&mut self, f: |&mut Vertices| -> (bool, U)) -> U {
        let (modified, ret) = f(&mut self.vertices);
        if modified {
            self.vbo_mod_count.bump();
        }
        ret
    }
    pub fn replace_indices(&mut self, indices: Indices) {
        use std::mem::replace;
        self.ibo_mod_count.bump();
        replace(&mut self.indices, indices);
    }
    // Return true in the first element of the tuple if you modify the indices in any way.
    pub fn modify_indices<U>(&mut self, f: |&mut Indices| -> (bool, U)) -> U {
        let (modified, ret) = f(&mut self.indices);
        if modified {
            self.ibo_mod_count.bump();
        }
        ret
    }
}
impl Clone for Mesh {
    fn clone(&self) -> Mesh {
        Mesh {
            ref_count:      Cell::new(0),
            vbo_mod_count:  Default::default(),
            ibo_mod_count:  Default::default(),

            vbo:            Cell::new(Default::default()),
            ibo:            Cell::new(Default::default()),

            vertices:       self.vertices.clone(),
            indices:        self.indices.clone(),
        }
    }
}
#[deriving(Clone)]
pub struct Vertices {
    // Should at least be one, having positions. Otherwise what's the point?
    attrs: Vec<TypedAttribute>,
    buf: Vec<u8>,

    // Size, in bytes, of a single vertex.
    element_size: uint,

    // Number of vertices.
    vertices_len: uint,
}
impl Vertices {
    pub fn stripped_attributes(&self) -> Vec<Attribute> {
        self.attrs.iter().map(|attr| attr.strip_type() ).collect()
    }
    // note this doesn't check that T && the types in attrs match in any way.
    // Also, it doesn't shrink the elements params.
    pub fn new<T>(mut elements: Vec<T>,
                  attrs: Vec<TypedAttribute>) -> Vertices {
        use std::mem::forget;
        let vert_len = elements.len();
        let elem_size = size_of::<T>();
        let buf = unsafe {
            Vec::from_raw_parts(vert_len * elem_size,
                                elements.capacity() * elem_size,
                                elements.as_mut_ptr() as *mut u8)
        };
        unsafe {
            // forget elements so it doesn't free our buffer once we return.
            forget(elements);
        }
        
        Vertices {
            attrs: attrs,
            buf: buf,
            element_size: elem_size,
            vertices_len: vert_len,
        }
    }

    // Returns (element byte offset, attribute).
    fn find_attr<'a>(attrs: &'a Vec<TypedAttribute>,
                     attr: Attribute) -> Option<(uint, &'a TypedAttribute)> {
        use super::traits::Type;
        let mut offset = 0;
        for a in attrs.iter() {
            if a.equiv(&attr) {
                return Some((offset, a));
            }
            offset += a.byte_size();
        }
        return None;
    }

    pub fn mut_attr_iter<'a>(&'a mut self, attr: Attribute) -> Option<MutAttrIter<'a>> {
        let &Vertices {
            buf: ref mut buf,
            attrs: ref attrs,
            element_size: element_size,
            vertices_len: vert_len,
            ..
        } = self;
        match Vertices::find_attr(attrs, attr) {
            Some((offset, attr)) => {
                Some(MutAttrIter {
                    buf: buf,
                    element_size: element_size,
                    vertices_len: vert_len,
                    attr: attr,
                    attr_offset: offset,
                    idx: 0,
                })
            }
            None => None,
        }
    }
    pub fn imm_attr_iter<'a>(&'a self, attr: Attribute) -> Option<ImmAttrIter<'a>> {
        let &Vertices {
            buf: ref buf,
            attrs: ref attrs,
            element_size: element_size,
            vertices_len: vert_len,
            ..
        } = self;
        match Vertices::find_attr(attrs, attr) {
            Some((offset, attr)) => {
                Some(ImmAttrIter {
                    buf: buf,
                    element_size: element_size,
                    vertices_len: vert_len,
                    attr: attr,
                    attr_offset: offset,
                    idx: 0,
                })
            }
            None => None,
        }
    }
}

pub struct ImmAttrIter<'vertices> {
    buf: &'vertices Vec<u8>,
    element_size: uint,
    vertices_len: uint,
    attr:  &'vertices TypedAttribute,
    attr_offset: uint,
    idx: uint,
}
pub struct MutAttrIter<'vertices> {
    buf: &'vertices mut Vec<u8>,
    element_size: uint,
    vertices_len: uint,
    attr:  &'vertices TypedAttribute,
    attr_offset: uint,
    idx: uint,
}
impl<'vertices> Iterator<TypedMutRef<'vertices>> for MutAttrIter<'vertices> {
    fn next(&mut self) -> Option<TypedMutRef<'vertices>> {
        use std::mem::transmute;
        let &MutAttrIter {
            buf: ref mut buf,
            element_size: elem_size,
            vertices_len: vert_len,
            attr: ref attr,
            attr_offset: attr_offset,
            idx: ref mut idx,
        } = self;
        if *idx >= vert_len {
            None
        } else {
            let last_idx = *idx;
            *idx += 1;
            let buf: &'vertices mut Vec<u8> = unsafe { transmute(buf) };
            let slice = buf.mut_slice(last_idx,
                                      elem_size);
            Some(attr.typed_mut_ref(slice, attr_offset))
        }
    }
}
impl<'vertices> Iterator<TypedImmRef<'vertices>> for ImmAttrIter<'vertices> {
    fn next(&mut self) -> Option<TypedImmRef<'vertices>> {
        if self.idx >= self.vertices_len {
            None
        } else {
            let last_idx = self.idx;
            self.idx += 1;
            Some(self.attr.typed_imm_ref(self.buf.slice(last_idx,
                                                        self.element_size),
                                         self.attr_offset))
        }
    }
}

impl Default for Vertices {
    fn default() -> Vertices {
        Vertices {
            attrs:        Vec::new(),
            buf:          Vec::new(),
            element_size: 0,
            vertices_len: 0,
        }
    }
}
#[deriving(Clone, Eq, PartialEq, Encodable, Decodable)]
pub struct SubMesh {
    slice_start: uint,
    slice_len:   uint,
    mode:        GeometryMode,
}
impl Collection for SubMesh {
    fn len(&self) -> uint {
        self.slice_len - self.slice_start
    }
}

#[deriving(Clone, Hash, Eq, PartialEq)]
pub enum TypedAttribute {
    PositionTypedAttribute(Type),
    NormalTypedAttribute(Type),
    TexCoordTypedAttribute(Type),
    
    CustomTypedAttribute(VertexLocus, Type),
}
impl super::traits::Type for TypedAttribute {
    fn byte_size(&self) -> uint {
        use super::traits::Type;
        self.attr_type().byte_size()
    }
}
impl cmp::Equiv<Attribute> for TypedAttribute {
    fn equiv(&self, other: &Attribute) -> bool {
        self.strip_type() == *other
    }
}
impl super::traits::Typed for TypedAttribute {
    fn expose_type<'a>(&'a self) -> &'a Type {
        match self {
            &PositionTypedAttribute(ref t) |
            &NormalTypedAttribute(ref t)   |
            &TexCoordTypedAttribute(ref t) |
            &CustomTypedAttribute(_, ref t) => t,
        }
    }
}
#[deriving(Clone, Hash, Eq, PartialEq)]
pub enum Attribute {
    PositionAttribute,
    NormalAttribute,
    TexCoordAttribute,

    CustomAttribute(VertexLocus),
}
impl TypedAttribute {
    pub fn default_position_attr() -> TypedAttribute {
        PositionTypedAttribute(Vec3Type(SingleFloatScalar))
    }
    pub fn default_normal_attr() -> TypedAttribute {
        NormalTypedAttribute(Vec3Type(SingleFloatScalar))
    }
    pub fn default_tex_coord_attr() -> TypedAttribute {
        TexCoordTypedAttribute(Vec2Type(SingleFloatScalar))
    }

    pub fn attr_type(&self) -> Type {
        match self {
            &PositionTypedAttribute(ref t) |
            &NormalTypedAttribute(ref t)   |
            &TexCoordTypedAttribute(ref t) |
            &CustomTypedAttribute(_, ref t) => t.clone(),
        }
    }
    pub fn strip_type(&self) -> Attribute {
        match self {
            &PositionTypedAttribute(_) => PositionAttribute,
            &NormalTypedAttribute(_)   => NormalAttribute,
            &TexCoordTypedAttribute(_) => TexCoordAttribute,
            &CustomTypedAttribute(ref locus, _) => CustomAttribute(locus.clone()),
        }
    }
    pub fn typed_imm_ref<'a>(&self, element: &'a [u8], element_offset: uint) -> TypedImmRef<'a> {
        self.attr_type().typed_imm_ref(element, element_offset)
    }
    pub fn typed_mut_ref<'a>(&self,
                             element: &'a mut [u8],
                             element_offset: uint) -> TypedMutRef<'a> {
        self.attr_type().typed_mut_ref(element, element_offset)
    }
}
impl Attribute {
    pub fn add_type(&self, t: Type) -> TypedAttribute {
        match self {
            &PositionAttribute => PositionTypedAttribute(t),
            &NormalAttribute   => NormalTypedAttribute(t),
            &TexCoordAttribute => TexCoordTypedAttribute(t),
            &CustomAttribute(ref locus) => CustomTypedAttribute(locus.clone(), t),
        }
    }
}
impl cmp::PartialOrd for Attribute {
    fn partial_cmp(&self, rhs: &Attribute) -> Option<cmp::Ordering> {
        Some(self.cmp(rhs))
    }
}
impl cmp::Ord for Attribute {
    fn cmp(&self, rhs: &Attribute) -> cmp::Ordering {
        use std::cmp::{Less, Equal, Greater};
        match self {
            &PositionAttribute => match rhs {
                &PositionAttribute => Greater,
                _ => Less,
            },
            &NormalAttribute => match rhs {
                &PositionAttribute => Greater,
                &NormalAttribute => Equal,
                _ => Less,
            },
            &TexCoordAttribute => match rhs {
                &PositionAttribute | &TexCoordAttribute => Greater,
                &NormalAttribute => Equal,
                _ => Greater,
            },
            &CustomAttribute(_) => match rhs {
                &PositionAttribute | &TexCoordAttribute | &NormalAttribute => Greater,
                _ => Equal,
            },
        }
    }
}

pub mod prefabs {
    use super::Handle;
    pub static PLANE_MESH: Handle = Handle(1);
}

pub struct Manager {
    next_id: u32,
    meshes: TreeMap<Handle, Mesh>,
}
impl Default for Manager {
    fn default() -> Manager {
        fn build_prefab_plane() -> Mesh {
            use math::vector::{Vector, Vec2, Vec3};

            let normal = Vector::wrap([0.0, 0.0, 1.0]);
            
            let verts: Vec<(Vec3, Vec3, Vec2)> = vec!(
                (Vector::wrap([-1.0, -1.0, 0.0]), // pos
                 normal,                          // normal
                 Vector::wrap([0.0,   1.0])),     // texcoord

                (Vector::wrap([1.0,  -1.0, 0.0]),
                 normal,
                 Vector::wrap([1.0,   1.0])),

                (Vector::wrap([1.0,   1.0, 0.0]),
                 normal,
                 Vector::wrap([1.0,   0.0])),

                (Vector::wrap([-1.0,  1.0, 0.0]),
                 normal,
                 Vector::wrap([0.0,   0.0])));

            let indices = Indices::new_simple(verts.len());

            let types = vec!(TypedAttribute::default_position_attr(),
                             TypedAttribute::default_normal_attr(),
                             TypedAttribute::default_tex_coord_attr());

            let verts = Vertices::new(verts, types);
            Mesh::new(verts, indices)
        }
        fn build_prefabs(mgr: &mut Manager) {
            let plane = build_prefab_plane();
            mgr.meshes.insert(prefabs::PLANE_MESH, plane);
        }
        let mut mgr = Manager {
            next_id: 100,
            meshes: TreeMap::new(),
        };
        build_prefabs(&mut mgr);
        mgr
    }
}
impl Manager {
    pub fn new() -> Manager {
        Default::default()
    }

    fn take_handle(&mut self) -> Handle {
        let id = self.next_id;
        self.next_id += 1;
        Handle(id)
    }
    pub fn clone_handle(&self, id: &Handle) -> Option<Handle> {
        self.meshes.find(id).map(|mesh| {
            let count = mesh.ref_count.get() + 1;
            mesh.ref_count.set(count);

            let &Handle(inner) = id;
            Handle(inner)
        })
    }
    pub fn drop_handle(&self, id: Handle) {
        self.meshes.find(&id).map(|mesh| {
            let count = mesh.ref_count.get();
            let count = if count > 0 {
                count - 1
            } else {
                count
            };
            mesh.ref_count.set(count);
        });
    }

    pub fn clone_mesh(&mut self, id: Handle) -> Option<Handle> {
        let new_id = self.take_handle();
        let mesh: Option<Mesh> = self.meshes.find(&id).map(|m| m.clone() );
        match mesh {
            Some(mesh) => {
                let mesh = mesh.clone();
                self.meshes.insert(new_id, mesh);
                Some(new_id)
            }
            None => None,
        }
    }

    pub fn create_mesh(&mut self, assemble: proc() -> Mesh) -> Handle {
        let id = self.take_handle();
        let mesh = assemble();
        self.meshes.insert(id, mesh);
        id
    }

    pub fn render(&self, ctxt: &mut Context3d, mesh: &Handle) {
        use ppapi::gles::UShortType;
        match self.meshes.find(mesh) {
            None => warn!("Unknown mesh handle: `{}`", *mesh),
            Some(mesh) => {
                let (_vbo_bound, ibo_bound) = mesh.clean_and_bind_buffers(ctxt);
                for sub in mesh.indices.subs.iter() {
                    ibo_bound.draw_elements(ctxt,
                                            sub.mode,
                                            UShortType,
                                            sub.slice_start,
                                            sub.slice_len);
                }
            }
        }
    }
}
