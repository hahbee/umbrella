use math;
use math::{Vec3, Vec2, Vec4, Vector3, Vector2};
use math::{Matrix4x4};
use math::Quaternion;
use math::{Pose, Orientation, Position};

use ppapi::gles;
pub use ppapi::gles::types;
use ppapi::gles::traits::{DropBuffer, Buffer};

use ppapi::Context3d;

use std::num::Zero;
use std::cmp;
use std::default::Default;
use collections::ringbuf::RingBuf;

use self::traits::Typed;

pub mod shader;
pub mod material;
pub mod mesh;
pub mod composer;
pub mod device;

#[deriving(Clone, Hash, Eq, PartialEq, Ord, PartialOrd, Encodable, Decodable)]
pub enum ScalarType {
    BooleanScalar,
    SignedIntegerScalar,
    SingleFloatScalar,
}
impl ScalarType {
    pub fn to_type(&self) -> Type {
        match self {
            &BooleanScalar       => BoolType,
            &SignedIntegerScalar => I32Type,
            &SingleFloatScalar   => F32Type,
        }
    }
}
#[deriving(Clone, Hash, Eq, PartialEq, Ord, PartialOrd, Encodable, Decodable)]
pub enum MatSizes {
    FourByFourMat,
    ThreeByThreeMat,
    TwoByTwoMat,
}
impl Collection for MatSizes {
    fn len(&self) -> uint {
        match self {
            &FourByFourMat => 16,
            &ThreeByThreeMat => 9,
            &TwoByTwoMat => 4,
        }
    }
}

#[deriving(Clone, Hash, Eq, PartialEq, Ord, PartialOrd, Encodable, Decodable)]
pub enum Type {
    BoolType,
    I32Type,
    F32Type,

    Vec2Type(ScalarType),
    Vec3Type(ScalarType),
    Vec4Type(ScalarType),

    // opengles 2 only supports 2x2, 3x3, or 4x4 float mats, hence the linear element.
    MatType(MatSizes),

    SamplerType(SamplerKind),

    // the following ~ is really unfortunate.
    ArrayType(Box<Type>, u8),
}
impl Type {
    pub fn typed_imm_ref<'a>(self, element: &'a [u8], offset: uint) -> TypedImmRef {
        fn cast<'b, T>(element: &'b [u8], offset: uint) -> &'b T {
            use std::mem::{size_of, transmute};
            let attr: &[T] = unsafe {
                transmute(element.slice(offset, size_of::<T>()))
            };
            attr.head().unwrap()
        }
        match self {
            BoolType => BoolImmRef(cast(element, offset)),
            I32Type  => I32ImmRef (cast(element, offset)),
            F32Type  => F32ImmRef (cast(element, offset)),

            Vec2Type(BooleanScalar)       => BVec2ImmRef(cast(element, offset)),
            Vec2Type(SignedIntegerScalar) => IVec2ImmRef(cast(element, offset)),
            Vec2Type(SingleFloatScalar)   => FVec2ImmRef(cast(element, offset)),

            Vec3Type(BooleanScalar)       => BVec3ImmRef(cast(element, offset)),
            Vec3Type(SignedIntegerScalar) => IVec3ImmRef(cast(element, offset)),
            Vec3Type(SingleFloatScalar)   => FVec3ImmRef(cast(element, offset)),

            Vec4Type(BooleanScalar)       => BVec4ImmRef(cast(element, offset)),
            Vec4Type(SignedIntegerScalar) => IVec4ImmRef(cast(element, offset)),
            Vec4Type(SingleFloatScalar)   => FVec4ImmRef(cast(element, offset)),

            _ => unimplemented!(),
        }
    }
    pub fn typed_mut_ref<'a>(self, element: &'a mut [u8], offset: uint) -> TypedMutRef {
        fn cast<T>(element: &mut [u8], offset: uint) -> &mut T {
            use std::mem::{size_of, transmute};
            let attr: &mut [T] = unsafe {
                transmute(element.slice(offset, size_of::<T>()))
            };
            unsafe {
                attr.unsafe_mut_ref(0)
            }
        }
        match self {
            BoolType => BoolMutRef(cast(element, offset)),
            I32Type  => I32MutRef (cast(element, offset)),
            F32Type  => F32MutRef (cast(element, offset)),

            Vec2Type(BooleanScalar)       => BVec2MutRef(cast(element, offset)),
            Vec2Type(SignedIntegerScalar) => IVec2MutRef(cast(element, offset)),
            Vec2Type(SingleFloatScalar)   => FVec2MutRef(cast(element, offset)),

            Vec3Type(BooleanScalar)       => BVec3MutRef(cast(element, offset)),
            Vec3Type(SignedIntegerScalar) => IVec3MutRef(cast(element, offset)),
            Vec3Type(SingleFloatScalar)   => FVec3MutRef(cast(element, offset)),

            Vec4Type(BooleanScalar)       => BVec4MutRef(cast(element, offset)),
            Vec4Type(SignedIntegerScalar) => IVec4MutRef(cast(element, offset)),
            Vec4Type(SingleFloatScalar)   => FVec4MutRef(cast(element, offset)),

            _ => unimplemented!(),
        }
    }

    pub fn is_sampler(&self) -> bool {
        match self {
            &SamplerType(_) => true,
            _ => false,
        }
    }

    pub fn glsl_typename(&self) -> &'static str {
        match self {
            &BoolType => "bool",
            &I32Type  => "int",
            &F32Type  => "float",

            &Vec2Type(BooleanScalar)       => "bvec2",
            &Vec2Type(SignedIntegerScalar) => "ivec2",
            &Vec2Type(SingleFloatScalar)   => "vec2",

            &Vec3Type(BooleanScalar)       => "bvec3",
            &Vec3Type(SignedIntegerScalar) => "ivec3",
            &Vec3Type(SingleFloatScalar)   => "vec3",

            &Vec4Type(BooleanScalar)       => "bvec4",
            &Vec4Type(SignedIntegerScalar) => "ivec4",
            &Vec4Type(SingleFloatScalar)   => "vec4",

            &MatType(TwoByTwoMat) => "mat2",
            &MatType(ThreeByThreeMat) => "mat3",
            &MatType(FourByFourMat) => "mat4",
            
            &SamplerType(Sampler2dKind) => "sampler2D",
            &SamplerType(SamplerCubeKind) => "samplerCube",

            _ => unimplemented!(),
        }
    }

    pub fn to_typed_opt_value(&self) -> TypedOptionalValue {
        match self {
            &BoolType          => BoolOptValue(None),
            &I32Type           => I32OptValue(None),
            &F32Type           => F32OptValue(None),

            &Vec2Type(BooleanScalar)       => BVec2OptValue(None),
            &Vec2Type(SignedIntegerScalar) => IVec2OptValue(None),
            &Vec2Type(SingleFloatScalar)   => IVec2OptValue(None),

            &Vec3Type(BooleanScalar)       => BVec3OptValue(None),
            &Vec3Type(SignedIntegerScalar) => IVec3OptValue(None),
            &Vec3Type(SingleFloatScalar)   => FVec3OptValue(None),

            &Vec4Type(BooleanScalar)       => BVec4OptValue(None),
            &Vec4Type(SignedIntegerScalar) => IVec4OptValue(None),
            &Vec4Type(SingleFloatScalar)   => FVec4OptValue(None),

            &MatType(TwoByTwoMat)          => unimplemented!(),
            &MatType(ThreeByThreeMat)      => unimplemented!(),
            &MatType(FourByFourMat)        => Mat4x4OptValue(None),
            
            &SamplerType(Sampler2dKind)    => Sampler2dOptValue(None),
            &SamplerType(SamplerCubeKind)  => unimplemented!(),

            &ArrayType(_, _)  => unimplemented!(),
        }
    }
}
impl Collection for Type {
    fn len(&self) -> uint {
        match self {
            &BoolType | &I32Type | &F32Type => 1,
            &Vec2Type(_) => 2,
            &Vec3Type(_) => 3,
            &Vec4Type(_) => 4,
            &MatType(size) => (size as uint) * (size as uint),
            &SamplerType(_) => 1,
            &ArrayType(_, count) => count as uint,
        }
    }
}

#[deriving(Clone, Hash, Eq, PartialEq, Ord, PartialOrd, Encodable, Decodable)]
pub enum SamplerKind {
    Sampler2dKind,
    SamplerCubeKind,
}

#[deriving(Clone, PartialEq, Encodable, Decodable)]
pub enum TypedValue {
    BoolValue(bool),
    I32Value(i32),
    F32Value(f32),
    
    BVec2Value((bool, bool)),
    IVec2Value((i32, i32)),
    FVec2Value(Vec2),

    BVec3Value((bool, bool, bool)),
    IVec3Value((i32, i32, i32)),
    FVec3Value(Vec3),

    BVec4Value((bool, bool, bool, bool)),
    IVec4Value((i32,  i32,  i32,  i32)),
    FVec4Value(Vec4),

    Mat4x4Value(Matrix4x4),

    // Note this stores the slot for this texture, not the texture buffer itself.
    Sampler2dValue(i32),
}
impl Collection for TypedValue {
    fn len(&self) -> uint {
        self.expose_type().len()
    }
}
impl TypedValue {
    pub fn to_typed_opt_value(&self) -> TypedOptionalValue {
        match self {
            &BoolValue(ref v)  => BoolOptValue(Some(v.clone())),
            &I32Value(ref v)   => I32OptValue(Some(v.clone())),
            &F32Value(ref v)   => F32OptValue(Some(v.clone())),
            &BVec2Value(ref v) => BVec2OptValue(Some(v.clone())),
            &IVec2Value(ref v) => IVec2OptValue(Some(v.clone())),
            &FVec2Value(ref v) => FVec2OptValue(Some(v.clone())),
            &BVec3Value(ref v) => BVec3OptValue(Some(v.clone())),
            &IVec3Value(ref v) => IVec3OptValue(Some(v.clone())),
            &FVec3Value(ref v) => FVec3OptValue(Some(v.clone())),
            &BVec4Value(ref v) => BVec4OptValue(Some(v.clone())),
            &IVec4Value(ref v) => IVec4OptValue(Some(v.clone())),
            &FVec4Value(ref v) => FVec4OptValue(Some(v.clone())),

            &Mat4x4Value(ref v) => Mat4x4OptValue(Some(v.clone())),
            &Sampler2dValue(ref v) => Sampler2dOptValue(Some(v.clone())),
        }
    }
}
#[deriving(Clone, PartialEq, Encodable, Decodable)]
pub enum TypedOptionalValue {
    BoolOptValue(Option<bool>),
    I32OptValue(Option<i32>),
    F32OptValue(Option<f32>),
    
    BVec2OptValue(Option<(bool, bool)>),
    IVec2OptValue(Option<(i32,  i32)>),
    FVec2OptValue(Option<Vec2>),

    BVec3OptValue(Option<(bool, bool, bool)>),
    IVec3OptValue(Option<(i32,  i32,  i32)>),
    FVec3OptValue(Option<Vec3>),

    BVec4OptValue(Option<(bool, bool, bool, bool)>),
    IVec4OptValue(Option<(i32,  i32,  i32,  i32)>),
    FVec4OptValue(Option<Vec4>),

    // FIXME: other mat sizes
    Mat4x4OptValue(Option<Matrix4x4>),

    // Note this stores the slot for this texture, not the texture buffer itself.
    Sampler2dOptValue(Option<i32>),
}
impl Collection for TypedOptionalValue {
    fn len(&self) -> uint {
        self.expose_type().len()
    }
}
impl TypedOptionalValue {
    pub fn is_none(&self) -> bool {
        match self {
            &BoolOptValue(ref v)  => v.is_none(),
            &I32OptValue(ref v)   => v.is_none(),
            &F32OptValue(ref v)   => v.is_none(),
            &BVec2OptValue(ref v) => v.is_none(),
            &IVec2OptValue(ref v) => v.is_none(),
            &FVec2OptValue(ref v) => v.is_none(),
            &BVec3OptValue(ref v) => v.is_none(),
            &IVec3OptValue(ref v) => v.is_none(),
            &FVec3OptValue(ref v) => v.is_none(),
            &BVec4OptValue(ref v) => v.is_none(),
            &IVec4OptValue(ref v) => v.is_none(),
            &FVec4OptValue(ref v) => v.is_none(),

            &Mat4x4OptValue(ref v) => v.is_none(),

            &Sampler2dOptValue(ref v) => v.is_none(),
        }
    }

    pub fn get_ref<'a>(&'a self) -> TypedImmRef<'a> {
        match self {
            &BoolOptValue(ref v)  => BoolImmRef(v.get_ref()),
            &I32OptValue(ref v)   => I32ImmRef(v.get_ref()),
            &F32OptValue(ref v)   => F32ImmRef(v.get_ref()),
            &BVec2OptValue(ref v) => BVec2ImmRef(v.get_ref()),
            &IVec2OptValue(ref v) => IVec2ImmRef(v.get_ref()),
            &FVec2OptValue(ref v) => FVec2ImmRef(v.get_ref()),
            &BVec3OptValue(ref v) => BVec3ImmRef(v.get_ref()),
            &IVec3OptValue(ref v) => IVec3ImmRef(v.get_ref()),
            &FVec3OptValue(ref v) => FVec3ImmRef(v.get_ref()),
            &BVec4OptValue(ref v) => BVec4ImmRef(v.get_ref()),
            &IVec4OptValue(ref v) => IVec4ImmRef(v.get_ref()),
            &FVec4OptValue(ref v) => FVec4ImmRef(v.get_ref()),

            &Mat4x4OptValue(ref v) => Mat4x4ImmRef(v.get_ref()),

            &Sampler2dOptValue(ref v) => Sampler2dImmRef(v.get_ref()),
        }
    }
}

pub enum TypedImmRef<'a> {
    BoolImmRef(&'a bool),
    I32ImmRef(&'a i32),
    F32ImmRef(&'a f32),
    
    BVec2ImmRef(&'a (bool, bool)),
    IVec2ImmRef(&'a (i32,  i32)),
    FVec2ImmRef(&'a Vec2),

    BVec3ImmRef(&'a (bool, bool, bool)),
    IVec3ImmRef(&'a (i32,  i32,  i32)),
    FVec3ImmRef(&'a Vec3),

    BVec4ImmRef(&'a (bool, bool, bool, bool)),
    IVec4ImmRef(&'a (i32,  i32,  i32,  i32)),
    FVec4ImmRef(&'a Vec4),

    Mat4x4ImmRef(&'a Matrix4x4),

    Sampler2dImmRef(&'a i32),
}
impl<'a> Collection for TypedImmRef<'a> {
    fn len(&self) -> uint {
        self.expose_type().len()
    }
}

impl<'a, 'b> cmp::PartialEq for TypedImmRef<'a> {
    fn eq(&self, rhs: &TypedImmRef<'b>) -> bool {
        match (self, rhs) {
            (&BoolImmRef(v1),  &BoolImmRef(v2))  if *v1 == *v2 => true,
            (&I32ImmRef(v1),   &I32ImmRef(v2))   if *v1 == *v2 => true,
            (&F32ImmRef(v1),   &F32ImmRef(v2))   if *v1 == *v2 => true,

            (&BVec2ImmRef(v1), &BVec2ImmRef(v2)) if *v1 == *v2 => true,
            (&IVec2ImmRef(v1), &IVec2ImmRef(v2)) if *v1 == *v2 => true,
            (&FVec2ImmRef(v1), &FVec2ImmRef(v2)) if *v1 == *v2 => true,

            (&BVec3ImmRef(v1), &BVec3ImmRef(v2)) if *v1 == *v2 => true,
            (&IVec3ImmRef(v1), &IVec3ImmRef(v2)) if *v1 == *v2 => true,
            (&FVec3ImmRef(v1), &FVec3ImmRef(v2)) if *v1 == *v2 => true,

            (&BVec4ImmRef(v1), &BVec4ImmRef(v2)) if *v1 == *v2 => true,
            (&IVec4ImmRef(v1), &IVec4ImmRef(v2)) if *v1 == *v2 => true,
            (&FVec4ImmRef(v1), &FVec4ImmRef(v2)) if *v1 == *v2 => true,

            _ => false,
        }
    }
}

pub enum TypedMutRef<'a> {
    BoolMutRef(&'a mut bool),
    I32MutRef(&'a mut i32),
    F32MutRef(&'a mut f32),

    BVec2MutRef(&'a mut (bool, bool)),
    IVec2MutRef(&'a mut (i32, i32)),
    FVec2MutRef(&'a mut Vec2),

    BVec3MutRef(&'a mut (bool, bool, bool)),
    IVec3MutRef(&'a mut (i32, i32, i32)),
    FVec3MutRef(&'a mut Vec3),

    BVec4MutRef(&'a mut (bool, bool, bool, bool)),
    IVec4MutRef(&'a mut (i32, i32, i32, i32)),
    FVec4MutRef(&'a mut Vec4),
}
impl<'a> Collection for TypedMutRef<'a> {
    fn len(&self) -> uint {
        self.expose_type().len()
    }
}

impl<'a, 'b> cmp::PartialEq for TypedMutRef<'a> {
    fn eq(&self, rhs: &TypedMutRef<'b>) -> bool {
        match (self, rhs) {
            (&BoolMutRef(ref v1),  &BoolMutRef(ref v2))  if **v1 == **v2 => true,
            (&I32MutRef(ref v1),   &I32MutRef(ref v2))   if **v1 == **v2 => true,
            (&F32MutRef(ref v1),   &F32MutRef(ref v2))   if **v1 == **v2 => true,

            (&BVec2MutRef(ref v1), &BVec2MutRef(ref v2)) if **v1 == **v2 => true,
            (&IVec2MutRef(ref v1), &IVec2MutRef(ref v2)) if **v1 == **v2 => true,
            (&FVec2MutRef(ref v1), &FVec2MutRef(ref v2)) if **v1 == **v2 => true,

            (&BVec3MutRef(ref v1), &BVec3MutRef(ref v2)) if **v1 == **v2 => true,
            (&IVec3MutRef(ref v1), &IVec3MutRef(ref v2)) if **v1 == **v2 => true,
            (&FVec3MutRef(ref v1), &FVec3MutRef(ref v2)) if **v1 == **v2 => true,

            (&BVec4MutRef(ref v1), &BVec4MutRef(ref v2)) if **v1 == **v2 => true,
            (&IVec4MutRef(ref v1), &IVec4MutRef(ref v2)) if **v1 == **v2 => true,
            (&FVec4MutRef(ref v1), &FVec4MutRef(ref v2)) if **v1 == **v2 => true,

            _ => false,
        }
    }
}

#[deriving(Eq, PartialEq, Hash, Clone, Encodable, Decodable)]
pub enum ModelViewStyle {
    Mat4x4MVS,
    QuatPosMVS,
}

#[deriving(Clone)]
pub struct Camera {
    pub far: f32,
    pub near: f32,
    pub aspect: f32,

    pub ivm: Matrix4x4,
    pub projection: Matrix4x4,

    pub position: math::Vec3,
    pub orientation: math::Quaternion,
    
    pub fixed_axis: math::Vec3,
}
impl Camera {
    pub fn new(aspect: f32, far: f32, near: f32) -> Camera {
        Camera {
            far: far,
            near: near,
            aspect: aspect,

            ivm: Matrix4x4::identity(),
            projection: Matrix4x4::new_projection(aspect, far, near),

            position: Zero::zero(),
            orientation: Default::default(),
            
            fixed_axis: Vector2::unit_y(),
        }
    }
    pub fn set_bindings(&self, _prog: &mut shader::Program) {
        
    }
    pub fn update_ivm(&mut self) {
        self.ivm = Matrix4x4::identity();
        let inv = self.orientation.rot_inverse();
        self.ivm.set_mut_rot_from_quat(inv);
        self.ivm.set_position(&inv.rotate_position(&self.position));
    }
    pub fn update_proj(&mut self) {
        self.projection = Matrix4x4::new_projection(self.aspect, self.near, self.far);
    }
    
    pub fn get_fixed_axis(&self) -> math::Vec3 {
        self.fixed_axis.clone()
    }
    pub fn set_fixed_axis(&mut self, axis: math::Vec3) {
        self.fixed_axis = axis;
    }
    pub fn look_at<T: math::Position>(&mut self, pos: &T) {
        let fixed = self.get_fixed_axis();
        self.look_at_mut(pos, &fixed);
    }
}
impl math::Position for Camera {
    fn get_position(&self) -> math::Vec3 {
        self.position.clone()
    }
    fn set_position<T: math::Position>(&mut self, pos: &T) {
        self.position = pos.get_position();
    } 
}
impl math::Orientation for Camera {
    fn rot_unit_invert(&mut self) {
        self.orientation.rot_unit_invert()
    }
    fn rot_invert(&mut self) {
        self.orientation.rot_invert()
    }
    fn rotate_position_inplace<T: Position>(&self, v: &mut T) {
        self.orientation.rotate_position_inplace(v)
    }

    fn set_mut_rot_from_quat(&mut self, q: Quaternion) {
        self.orientation.set_mut_rot_from_quat(q)
    }
    fn set_mut_rot_from_axes<X: Vector3, Y: Vector3, Z: Vector3>(&mut self,
                                                                 x: X, y: Y, z: Z) {
        self.orientation.set_mut_rot_from_axes(x, y, z)
    }
}
impl math::Pose for Camera {
}

// a queue of buffers we're done with (used in drop, where we don't have a context).
local_data_key!(buffer_drop_queue: RingBuf<gles::BufferObject>)

pub fn enqueue_for_disposal<T: DropBuffer + Buffer + Clone>(globj: T) {
    use std::collections::Deque;
    let queue_opt = buffer_drop_queue.replace(None);
    let mut queue = queue_opt.unwrap_or_else(|| RingBuf::new() );
    queue.push_front(globj.to_object());
    buffer_drop_queue.replace(Some(queue));
}
pub fn process_disposal_queue(ctxt: &Context3d) {
    use std::collections::Deque;
    match buffer_drop_queue.replace(Some(RingBuf::new())) {
        Some(mut queue) => {
            queue.pop().while_some(|buf_obj| {
                unsafe {
                    buf_obj.drop_buffer(ctxt);
                }
                queue.pop()
            });
        }
        None => (),
    }
}

pub mod traits {
    pub trait Type {
        fn byte_size(&self) -> uint;
    }
    impl Type for super::ScalarType {
        fn byte_size(&self) -> uint {
            use super::{BooleanScalar, SignedIntegerScalar, SingleFloatScalar};
            match self {
                &BooleanScalar => 1,
                &SignedIntegerScalar => 4,
                &SingleFloatScalar => 4,
            }
        }
    }
    impl Type for super::Type {
        fn byte_size(&self) -> uint {
            use super::{BoolType, I32Type, F32Type,
                        Vec2Type, Vec3Type, Vec4Type,
                        MatType, ArrayType, SamplerType};
            match self {
                &BoolType => 1,
                &I32Type | &F32Type => 4,
                &Vec2Type(scalar) => scalar.byte_size() * 2,
                &Vec3Type(scalar) => scalar.byte_size() * 3,
                &Vec4Type(scalar) => scalar.byte_size() * 4,
                &MatType(size) => 4 * (size as uint) * (size as uint),
                &SamplerType(_) => unreachable!(), // FIXME
                &ArrayType(ref t, count) => t.byte_size() * (count as uint),
            }
        }
    }
    impl Type for super::TypedValue {
        fn byte_size(&self) -> uint {
            use super::{BoolValue, I32Value, F32Value,
                        BVec2Value, IVec2Value, FVec2Value,
                        BVec3Value, IVec3Value, FVec3Value,
                        BVec4Value, IVec4Value, FVec4Value,
                        Mat4x4Value, Sampler2dValue};
            match self {
                &BoolValue(_) => 1,
                & I32Value(_) | &F32Value(_) => 4,
                &BVec2Value(_) => 2,
                &IVec2Value(_) | &FVec2Value(_) => 8,

                &BVec3Value(_) => 3,
                &IVec3Value(_) | &FVec3Value(_) => 12,

                &BVec4Value(_) => 4,
                &IVec4Value(_) | &FVec4Value(_) => 16,

                &Mat4x4Value(_) => 64,

                &Sampler2dValue(_) => 4,
            }
        }
    }
    impl<'a> Type for super::TypedImmRef<'a> {
        fn byte_size(&self) -> uint {
            use super::{BoolImmRef, I32ImmRef, F32ImmRef,
                        BVec2ImmRef, IVec2ImmRef, FVec2ImmRef,
                        BVec3ImmRef, IVec3ImmRef, FVec3ImmRef,
                        BVec4ImmRef, IVec4ImmRef, FVec4ImmRef,
                        Mat4x4ImmRef, Sampler2dImmRef};
            match self {
                &BoolImmRef(_) => 1,
                & I32ImmRef(_) | &F32ImmRef(_) => 4,

                &BVec2ImmRef(_) => 2,
                &IVec2ImmRef(_) | &FVec2ImmRef(_) => 8,

                &BVec3ImmRef(_) => 3,
                &IVec3ImmRef(_) | &FVec3ImmRef(_) => 12,

                &BVec4ImmRef(_) => 4,
                &IVec4ImmRef(_) | &FVec4ImmRef(_) => 16,

                &Mat4x4ImmRef(_) => 64,

                &Sampler2dImmRef(_) => 4,
            }
        }
    }
    impl<'a> Type for super::TypedMutRef<'a> {
        fn byte_size(&self) -> uint {
            use super::{BoolMutRef, I32MutRef, F32MutRef,
                        BVec2MutRef, IVec2MutRef, FVec2MutRef,
                        BVec3MutRef, IVec3MutRef, FVec3MutRef,
                        BVec4MutRef, IVec4MutRef, FVec4MutRef};
            match self {
                &BoolMutRef(_) => 1,
                & I32MutRef(_) | &F32MutRef(_) => 4,

                &BVec2MutRef(_) => 2,
                &IVec2MutRef(_) | &FVec2MutRef(_) => 8,

                &BVec3MutRef(_) => 3,
                &IVec3MutRef(_) | &FVec3MutRef(_) => 12,

                &BVec4MutRef(_) => 4,
                &IVec4MutRef(_) | &FVec4MutRef(_) => 16,
            }
        }
    }

    // A type that has type info associated with it.
    pub trait Typed {
        fn expose_type<'a>(&'a self) -> &'a super::Type;

        fn identical_type<T: Typed>(&self, rhs: &T) -> bool {
            *self.expose_type() == *rhs.expose_type()
        }
    }
    impl Typed for super::ScalarType {
        fn expose_type<'a>(&'a self) -> &'a super::Type {
            use super::{BooleanScalar, SignedIntegerScalar, SingleFloatScalar};
            match self {
                &BooleanScalar       => &BOOL_VALUE_TYPE,
                &SignedIntegerScalar => &I32_VALUE_TYPE,
                &SingleFloatScalar   => &F32_VALUE_TYPE,
            }
        }
    }
    impl Typed for super::Type {
        // One of the few times were always_inline is acceptable.
        #[inline(always)]
        fn expose_type<'a>(&'a self) -> &'a super::Type {
            self
        }
    }

    static BOOL_VALUE_TYPE:  super::Type = super::BoolType;
    static I32_VALUE_TYPE:   super::Type = super::I32Type;
    static F32_VALUE_TYPE:   super::Type = super::F32Type;
    static BVEC2_VALUE_TYPE: super::Type = super::Vec2Type(super::BooleanScalar);
    static IVEC2_VALUE_TYPE: super::Type = super::Vec2Type(super::SignedIntegerScalar);
    static FVEC2_VALUE_TYPE: super::Type = super::Vec2Type(super::SingleFloatScalar);
    static BVEC3_VALUE_TYPE: super::Type = super::Vec3Type(super::BooleanScalar);
    static IVEC3_VALUE_TYPE: super::Type = super::Vec3Type(super::SignedIntegerScalar);
    static FVEC3_VALUE_TYPE: super::Type = super::Vec3Type(super::SingleFloatScalar);
    static BVEC4_VALUE_TYPE: super::Type = super::Vec4Type(super::BooleanScalar);
    static IVEC4_VALUE_TYPE: super::Type = super::Vec4Type(super::SignedIntegerScalar);
    static FVEC4_VALUE_TYPE: super::Type = super::Vec4Type(super::SingleFloatScalar);
    static MAT4X4_VALUE_TYPE: super::Type = super::MatType(super::FourByFourMat);
    static SAMPLER2D_VALUE_TYPE: super::Type = super::SamplerType(super::Sampler2dKind);

    impl Typed for super::TypedValue {
        fn expose_type<'a>(&'a self) -> &'a super::Type {
            use super::{BoolValue, I32Value, F32Value,
                        BVec2Value, IVec2Value, FVec2Value,
                        BVec3Value, IVec3Value, FVec3Value,
                        BVec4Value, IVec4Value, FVec4Value,
                        Mat4x4Value, Sampler2dValue};
            match self {
                &BoolValue(_)  => &BOOL_VALUE_TYPE,
                &I32Value(_)   => &I32_VALUE_TYPE,
                &F32Value(_)   => &F32_VALUE_TYPE,
                &BVec2Value(_) => &BVEC2_VALUE_TYPE,
                &IVec2Value(_) => &IVEC2_VALUE_TYPE,
                &FVec2Value(_) => &FVEC2_VALUE_TYPE,
                &BVec3Value(_) => &BVEC3_VALUE_TYPE,
                &IVec3Value(_) => &IVEC3_VALUE_TYPE,
                &FVec3Value(_) => &FVEC3_VALUE_TYPE,
                &BVec4Value(_) => &BVEC4_VALUE_TYPE,
                &IVec4Value(_) => &IVEC4_VALUE_TYPE,
                &FVec4Value(_) => &FVEC4_VALUE_TYPE,

                &Mat4x4Value(_) => &MAT4X4_VALUE_TYPE,
                &Sampler2dValue(_) => &SAMPLER2D_VALUE_TYPE,
            }
        }
    }
    impl Typed for super::TypedOptionalValue {
        fn expose_type<'a>(&'a self) -> &'a super::Type {
            use super::{BoolOptValue, I32OptValue, F32OptValue,
                        BVec2OptValue, IVec2OptValue, FVec2OptValue,
                        BVec3OptValue, IVec3OptValue, FVec3OptValue,
                        BVec4OptValue, IVec4OptValue, FVec4OptValue,
                        Mat4x4OptValue, Sampler2dOptValue};
            match self {
                &BoolOptValue(_)  => &BOOL_VALUE_TYPE,
                &I32OptValue(_)   => &I32_VALUE_TYPE,
                &F32OptValue(_)   => &F32_VALUE_TYPE,
                &BVec2OptValue(_) => &BVEC2_VALUE_TYPE,
                &IVec2OptValue(_) => &IVEC2_VALUE_TYPE,
                &FVec2OptValue(_) => &FVEC2_VALUE_TYPE,
                &BVec3OptValue(_) => &BVEC3_VALUE_TYPE,
                &IVec3OptValue(_) => &IVEC3_VALUE_TYPE,
                &FVec3OptValue(_) => &FVEC3_VALUE_TYPE,
                &BVec4OptValue(_) => &BVEC4_VALUE_TYPE,
                &IVec4OptValue(_) => &IVEC4_VALUE_TYPE,
                &FVec4OptValue(_) => &FVEC4_VALUE_TYPE,

                &Mat4x4OptValue(_) => &MAT4X4_VALUE_TYPE,
                &Sampler2dOptValue(_) => &SAMPLER2D_VALUE_TYPE,
            }
        }
    }
    impl<'a> Typed for super::TypedImmRef<'a> {
        fn expose_type<'a>(&'a self) -> &'a super::Type {
            use super::{BoolImmRef, I32ImmRef, F32ImmRef,
                        BVec2ImmRef, IVec2ImmRef, FVec2ImmRef,
                        BVec3ImmRef, IVec3ImmRef, FVec3ImmRef,
                        BVec4ImmRef, IVec4ImmRef, FVec4ImmRef,
                        Mat4x4ImmRef, Sampler2dImmRef};
            match self {
                &BoolImmRef(_)  => &BOOL_VALUE_TYPE,
                &I32ImmRef(_)   => &I32_VALUE_TYPE,
                &F32ImmRef(_)   => &F32_VALUE_TYPE,
                &BVec2ImmRef(_) => &BVEC2_VALUE_TYPE,
                &IVec2ImmRef(_) => &IVEC2_VALUE_TYPE,
                &FVec2ImmRef(_) => &FVEC2_VALUE_TYPE,
                &BVec3ImmRef(_) => &BVEC3_VALUE_TYPE,
                &IVec3ImmRef(_) => &IVEC3_VALUE_TYPE,
                &FVec3ImmRef(_) => &FVEC3_VALUE_TYPE,
                &BVec4ImmRef(_) => &BVEC4_VALUE_TYPE,
                &IVec4ImmRef(_) => &IVEC4_VALUE_TYPE,
                &FVec4ImmRef(_) => &FVEC4_VALUE_TYPE,

                &Mat4x4ImmRef(_) => &MAT4X4_VALUE_TYPE,
                &Sampler2dImmRef(_) => &SAMPLER2D_VALUE_TYPE,
            }
        }
    }
    impl<'a> Typed for super::TypedMutRef<'a> {
        fn expose_type<'a>(&'a self) -> &'a super::Type {
            use super::{BoolMutRef, I32MutRef, F32MutRef,
                        BVec2MutRef, IVec2MutRef, FVec2MutRef,
                        BVec3MutRef, IVec3MutRef, FVec3MutRef,
                        BVec4MutRef, IVec4MutRef, FVec4MutRef};
            match self {
                &BoolMutRef(_)  => &BOOL_VALUE_TYPE,
                &I32MutRef(_)   => &I32_VALUE_TYPE,
                &F32MutRef(_)   => &F32_VALUE_TYPE,
                &BVec2MutRef(_) => &BVEC2_VALUE_TYPE,
                &IVec2MutRef(_) => &IVEC2_VALUE_TYPE,
                &FVec2MutRef(_) => &FVEC2_VALUE_TYPE,
                &BVec3MutRef(_) => &BVEC3_VALUE_TYPE,
                &IVec3MutRef(_) => &IVEC3_VALUE_TYPE,
                &FVec3MutRef(_) => &FVEC3_VALUE_TYPE,
                &BVec4MutRef(_) => &BVEC4_VALUE_TYPE,
                &IVec4MutRef(_) => &IVEC4_VALUE_TYPE,
                &FVec4MutRef(_) => &FVEC4_VALUE_TYPE,
            }
        }
    }
}
