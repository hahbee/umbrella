use std::str::{MaybeOwned, Slice};
use std::vec::Vec;
use std::collections::Collection;
use std::cell::RefCell;
use std::cmp::{PartialEq};
use std::c_str::{CString};
use std::rc::{Rc, Weak};

use ppapi::gles::traits::CompileShader;
use ppapi::gles::{ShaderProgram, FragmentShader, VertexShader, BoundShaderProgram};
use ppapi::Context3d;

use infra::{Key, DirtyFlag};

use super::types;
use super::{Type, TypedOptionalValue, TypedValue, SamplerKind};
use super::mesh::TypedAttribute;
use super::traits::Typed;

use std::collections::{HashMap};

#[deriving(Eq, PartialEq, Ord, PartialOrd, Clone, Hash)]
pub struct VertHandle(types::UInt);
#[deriving(Eq, PartialEq, Ord, PartialOrd, Clone, Hash)]
pub struct FragHandle(types::UInt);
#[deriving(Eq, PartialEq, Ord, PartialOrd, Clone, Hash)]
pub struct Handle(u32);

#[deriving(Eq, PartialEq, Ord, PartialOrd, Clone, Hash)]
pub enum VertexLocus {
    IndexVertLocus(u32),
    NameVertLocus(MaybeOwned<'static>),
}

#[deriving(Eq, PartialEq, Ord, PartialOrd, Clone, Hash)]
pub enum FragLocus {
    IndexFragLocus(u32),
    NameFragLocus(MaybeOwned<'static>),
}

#[deriving(PartialEq, Clone, Encodable, Decodable)]
pub enum RValDatum {
    // An input of the shader, usually an uniform.
    InputDatum(UniformRole),

    // A variable local to the scope, as a right-value.
    VarRValDatum(Key, Type),

    // An output of the shader as a RValDatum.
    OutputRValDatum(Key, Type),

    ConstDatum(TypedValue),
}
impl RValDatum {
    pub fn key(&self) -> Option<Key> {
        match self {
            &InputDatum(_) => None,
            &VarRValDatum(ref k, _) |
            &OutputRValDatum(ref k, _) => Some(k.clone()),
            &ConstDatum(_) => None,
        }
    }
}
impl Collection for RValDatum {
    fn len(&self) -> uint {
        self.expose_type().len()
    }
}
impl Typed for RValDatum {
    fn expose_type<'a>(&'a self) -> &'a Type {
        match self {
            &InputDatum(ref role) => role.expose_type(),
            &VarRValDatum(_, ref t) |
            &OutputRValDatum(_, ref t) => t,
            &ConstDatum(ref tv) => tv.expose_type(),
        }
    }
}
impl<'a> Typed for &'a RValDatum {
    fn expose_type<'a>(&'a self) -> &'a Type {
        match self {
            & &InputDatum(ref role) => role.expose_type(),
            & &VarRValDatum(_, ref t) |
            & &OutputRValDatum(_, ref t) => t,
            & &ConstDatum(ref tv) => tv.expose_type(),
        }
    }
}

#[deriving(Eq, PartialEq, Clone, Hash, Encodable, Decodable)]
pub enum LValDatum {
    // A variable local to the scope, as a left-value.
    VarLValDatum(Type),

    UpValueLValDatum(Key, Type),

    // An output of the shader.
    OutputLValDatum(Key, Type),
}
impl Collection for LValDatum {
    fn len(&self) -> uint {
        self.expose_type().len()
    }
}
impl Typed for LValDatum {
    fn expose_type<'a>(&'a self) -> &'a Type {
        match self {
            &VarLValDatum(ref t) |
            &UpValueLValDatum(_, ref t) |
            &OutputLValDatum(_, ref t) => t,
        }
    }
}
impl<'a> Typed for &'a LValDatum {
    fn expose_type<'a>(&'a self) -> &'a Type {
        match self {
            & &VarLValDatum(ref t) |
            & &UpValueLValDatum(_, ref t) |
            & &OutputLValDatum(_, ref t) => t,
        }
    }
}

#[deriving(Eq, PartialEq, Ord, PartialOrd, Clone, Hash, Encodable, Decodable)]
pub enum BinOpOp {
    AddBinOp,
    SubBinOp,
    MulBinOp,
    DivBinOp,

    EqBinOp,
    NotEqBinOp,
    LtBinOp,
    GtBinOp,
    LtEqBinOp,
    GtEqBinOp,
}
#[deriving(Eq, PartialEq, Ord, PartialOrd, Clone, Hash, Encodable, Decodable)]
pub enum UnaryOpOP {
    NegateUnaryOp,

    // clone the result of an expression.
    AssignUnaryOp,

    // Not, strictly speaking, an unary op; le shrug.
    IndexUnaryOp(uint),
}
#[deriving(Clone, Encodable, Decodable)]
pub enum Expression {
    SwizzleExpr {
        pub dest: LValDatum,
        pub src:  RValDatum,
        // indices to the compound element
        pub mask: Vec<uint>,
    },
    FunctionalExpr {
        pub dest: Option<LValDatum>,
        pub fun:  String,
        pub args: Vec<RValDatum>,
    },

    // An if branch. Notice there isn't a dest datum.
    BranchExpr {
        pub dest: Option<LValDatum>,
        pub cond: (BinOpOp, RValDatum, RValDatum),
        pub if_true: Block,
        pub if_false: Block,
    },

    BinOpExpr {
        pub dest: LValDatum,
        pub op: BinOpOp,
        pub left: RValDatum,
        pub right: RValDatum,
    },

    UnaryOpExpr {
        pub dest: LValDatum,
        pub op: UnaryOpOP,
        pub src: RValDatum,
    }
}

pub type ExpressionVerificationResult = Result<(), &'static str>;

impl Expression {
    // verify that we are following the rules.
    fn verify(&self) -> ExpressionVerificationResult {
        static TYPE_MISMATCH: ExpressionVerificationResult = Err("type mismatch");
        static OK: ExpressionVerificationResult = Ok(());

        fn identical_types<T: Typed, U: Typed>(lhs: &Option<T>,
                                               rhs: &Option<U>) -> ExpressionVerificationResult {
            if lhs.is_none() && rhs.is_none() {
                OK
            } else if !(lhs.is_some() && rhs.is_some()) ||
                !lhs.get_ref().identical_type(rhs.get_ref()) {
                    TYPE_MISMATCH
                } else {
                    OK
                }
        }
        fn verify_binop(_op: &BinOpOp,
                        lhs: &RValDatum,
                        rhs: &RValDatum) -> ExpressionVerificationResult {
            identical_types(&Some(lhs), &Some(rhs))
                .and_then(|()| {
                    if lhs.expose_type().is_sampler() {
                        // We don't need to check rhs because identical_types
                        // will ensure we have the same types.
                        Err("sampler in binop?")
                    } else {
                        OK
                    }
                })
        }

        match self {
            &SwizzleExpr {
                mask: ref m, ..
            } if m.len() == 0 => Err("swizzle must have a mask"),
            &SwizzleExpr {
                dest: ref d,
                mask: ref m,
                ..
            } if m.len() != d.len() => Err("swizzle mask length does not match dest type"),
            &SwizzleExpr {
                dest: ref d,
                src: ref s,
                ..
            } if s.expose_type().is_sampler() ||
                d.expose_type().is_sampler() => Err("samplers can't be swizzled"),
            &SwizzleExpr { .. } => OK,

            // Nothing to check, yet.
            &FunctionalExpr { .. } => OK,

            &BranchExpr {
                dest: ref dest,
                cond: (ref op, ref lhs, ref rhs),
                if_true: Block {
                    ret: ref if_true, ..
                },
                if_false: Block {
                    ret: ref if_false, ..
                },
                ..
            } => identical_types(dest, if_true)
                .and_then(|()| identical_types(dest, if_false) )
                .and_then(|()| verify_binop(op, lhs, rhs) ),

            &BinOpExpr {
                dest: ref d,
                op:   ref op,
                left: ref l,
                right: ref r,
            } => identical_types(&Some(d), &Some(l))
                .and_then(|()| verify_binop(op, l, r) ),
            
            &UnaryOpExpr {
                dest: ref d,
                src:  ref s,
                ..
            } => identical_types(&Some(d), &Some(s)),
        }
    }
       
}

// Every block is its own scope (as one would expect). However, departing from
// the usual, I choose for simplicity's sake, to impl blocks as if they were
// closures. As such, all Block's have their own key-space.
#[deriving(Clone, Encodable, Decodable)]
pub struct Block {
    // An array of vars that we have closed over.
    upvalues: Vec<Key>,

    exprs: Vec<(Option<Key>, Expression)>,
    ret:   Option<RValDatum>,
}
impl Block {
    pub fn new() -> Block {
        Block {
            upvalues: Vec::new(),
            exprs: Vec::new(),
            ret:   None,
        }
    }

    pub fn set_ret(&mut self, datum: RValDatum) {
        self.ret = Some(datum);
    }

    fn next_key(&self) -> Key {
        use std::iter::range;
        let mut key = None;
        for i in range(0, self.exprs.len()) {
            match self.exprs[self.exprs.len() - i] {
                (Some(k), _) => {
                    key = Some(k + 1);
                    break;
                }
                _ => {}
            }
        }
        key.unwrap_or_else(|| Key(0) )
    }

    // The caller is responsible for opening & closing scope blocks.
    fn build_with_upvalues(&self, upvalues: &Vec<Key>) -> Vec<String> {
        unimplemented!()
        Vec::new()
    }
}

pub trait ExpressionBlock {
    fn push_expr(&mut self, expr: Expression) -> Result<Option<RValDatum>, &'static str>;
    fn has_ret(&self) -> bool;
}
impl ExpressionBlock for Block {
    fn push_expr(&mut self, expr: Expression) -> Result<Option<RValDatum>, &'static str> {
        try!(expr.verify());

        let (datum, key) = match expr {
            SwizzleExpr {
                dest: ref d, ..
            } | BinOpExpr {
                dest: ref d, ..
            } | UnaryOpExpr {
                dest: ref d, ..
            } | FunctionalExpr {
                dest: Some(ref d), ..
            } | BranchExpr  {
                dest: Some(ref d), ..
            } => match d {
                &UpValueLValDatum(_, _) => unimplemented!(),
                &VarLValDatum(ref t) => {
                    let key = self.next_key();
                    (Some(VarRValDatum(key.clone(),
                                       t.clone())),
                     Some(key))
                }
                &OutputLValDatum(ref k, ref t) => {
                    (Some(OutputRValDatum(k.clone(),
                                          t.clone())),
                     None)
                }
            },
            _ => (None, None),
        };

        self.exprs.push((key, expr));
        Ok(datum)
    }
    fn has_ret(&self) -> bool {
        self.ret.is_some()
    }
}

pub mod vert {
    pub mod input {
        use infra::Key;
        use gl::{Type, MatType, Vec3Type, Vec2Type, SingleFloatScalar, FourByFourMat};
        use std::uint::MAX;
        pub static VIEW_MATRIX_UNIFORM: (Key, Type) = (Key(0), MatType(FourByFourMat));
        pub static PROJECTION_MATRIX_UNIFORM: (Key, Type) = (Key(1), MatType(FourByFourMat));

        // Notice the lack of types here. This is so we may conveniently handle
        // lower dimension coordinate systems. When we build the source for our
        // shader, we grab the types for these from the mesh.
        pub static POSITION_ATTRIBUTE_KEY: Key = Key(2);
        pub static NORMAL_ATTRIBUTE_KEY:   Key = Key(3);
        pub static TEXCOORD_ATTRIBUTE_KEY: Key = Key(4);

        // For convenience, these have types that one would usually see.
        pub static POSITION_ATTRIBUTE: (Key, Type) = (POSITION_ATTRIBUTE_KEY,
                                                      Vec3Type(SingleFloatScalar));
        pub static NORMAL_ATTRIBUTE:   (Key, Type) = (NORMAL_ATTRIBUTE_KEY,
                                                      Vec3Type(SingleFloatScalar));
        pub static TEXCOORD_ATTRIBUTE: (Key, Type) = (TEXCOORD_ATTRIBUTE_KEY,
                                                      Vec2Type(SingleFloatScalar));

        pub static USER_INPUT_START: Key = Key(MAX / 2u);
    }
    pub mod output {
        use infra::Key;
        use gl::{Type, Vec3Type, SingleFloatScalar, F32Type};
        pub static POSITION: (Key, Type) = (Key(0), Vec3Type(SingleFloatScalar));
        pub static POINT_SIZE: (Key, Type) = (Key(1), F32Type);
    }
}
pub mod frag {
    pub mod input {
        use infra::Key;
        use gl::{Type, BoolType, Vec4Type, SingleFloatScalar, I32Type};
        use super::super::vert;

        pub static COORD: (Key, Type) = (Key(0), Vec4Type(SingleFloatScalar));
        pub static FRONT_FACING: (Key, Type) = (Key(1), BoolType);
        pub static POINT_COORD:  (Key, Type) = (Key(2), I32Type);

        

        pub static USER_INPUT_START: Key = vert::input::USER_INPUT_START;
    }
    pub mod output {
        use infra::Key;
        use gl::{Type, Vec4Type, SingleFloatScalar};
        
        pub static COLOR: (Key, Type) = (Key(0), Vec4Type(SingleFloatScalar));
        pub fn data_attachment(index: uint) -> (Key, Type) {
            // Leave room between our data attachment keys and COLOR above
            // for additionally outputs. For now, I'm using 20.
            (Key(21) + index, Vec4Type(SingleFloatScalar))
        }
    }
}

// At some point, Role's will be used for plugin rendering effects.
#[deriving(Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Encodable, Decodable)]
pub enum UniformRole {
    CustomUniformRole(Key, Type),
    ModelViewUniformRole,
    ProjMatrixUniformRole,
    SamplerUniformRole(SamplerKind,
                       uint), // an index for multitexturing
}
impl Typed for UniformRole {
    fn expose_type<'a>(&'a self) -> &'a Type {
        use super::{MatType, I32Type, FourByFourMat};
        static MAT4: Type = MatType(FourByFourMat);
        static I32:  Type = I32Type;

        // Note we're exposing the type used within the function, not
        // the types of the uniforms themselves.
        match self {
            &CustomUniformRole(_, ref t) => t,
            &ModelViewUniformRole |
            &ProjMatrixUniformRole => &MAT4,
            &SamplerUniformRole(_, _) => &I32,
        }
    }
}

pub type UniformBindings = HashMap<UniformRole, BindingsEntry>;
// A runtime instance of a complete, linked shader program.
pub struct Program {
    mgr: WeakManager,
    prog: ShaderProgram,
    bindings: UniformBindings,
}
impl Program {
    // Note: fails if v's type is not the same as the key's type
    // Does nothing if the key isn't found.
    pub fn set(&mut self, k: &UniformRole, v: TypedValue) {
        assert!(k.identical_type(&v));
        match self.bindings.find_mut(k) {
            Some(tv) => tv.value.set(v.to_typed_opt_value()),
            None => {}
        }
    }
    pub fn maybe_set(&mut self, k: &UniformRole, v: TypedValue) {
        assert!(k.identical_type(&v));
        match self.bindings.find_mut(k) {
            Some(tv) => tv.value.maybe_set(v.to_typed_opt_value()),
            None => {}
        }
    }

    pub fn bind(&self, ctxt: &mut Context3d) {
        use super::{I32ImmRef, F32ImmRef, IVec2ImmRef, FVec2ImmRef,
                    IVec3ImmRef, FVec3ImmRef, IVec4ImmRef, FVec4ImmRef,
                    Mat4x4ImmRef, Sampler2dImmRef};
        use math::vector::Vector;
        use std::slice::ref_slice;
        use std::tuple::{Tuple2, Tuple3, Tuple4};

        let mut bound: BoundShaderProgram = self.prog.use_program(ctxt);

        for (_, entry) in self.bindings.iter() {
            if entry.value.is_dirty() && !entry.value.get_ref().is_none() {
                // Damn. This is longer than I expected when I started.
                match entry.value.get_ref().get_ref() {
                    /*BoolImmRef(r)  =>
                        bound.uniform(ctxt,
                                      Some(entry.index),
                                      ref_slice(r)),
                    BVec2ImmRef(r) =>
                        bound.uniform(ctxt,
                                      Some(entry.index),
                                      r.as_slice()),
                    BVec3ImmRef(r) =>
                        bound.uniform(ctxt,
                                      Some(entry.index),
                                      r.as_slice()),
                    BVec4ImmRef(r) =>
                        bound.uniform(ctxt,
                                      Some(entry.index),
                                      r.as_slice()),*/

                    I32ImmRef(r)   =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      ref_slice(r)),
                    IVec2ImmRef(r) =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      &[r.val0(), r.val1()]),
                    IVec3ImmRef(r) =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      &[r.val0(), r.val1(), r.val2()]),
                    IVec4ImmRef(r) =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      &[r.val0(), r.val1(), r.val2(), r.val3()]),

                    F32ImmRef(r)   =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      ref_slice(r)),
                    FVec2ImmRef(r) =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      r.as_slice()),
                    FVec3ImmRef(r) =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      r.as_slice()),
                    FVec4ImmRef(r) =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      r.as_slice()),

                    Mat4x4ImmRef(r) =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      r.as_slice()),

                    Sampler2dImmRef(r) =>
                        bound.uniform(ctxt,
                                      entry.index,
                                      ref_slice(r)),

                    _ => unimplemented!()
                }
                entry.value.reset();
            }
        }
    }
}

#[deriving(Clone)]
struct BindingsEntry {
    value: DirtyFlag<TypedOptionalValue>,
    index: Option<i32>,
}

struct ShaderStore {
    src: Vec<MaybeOwned<'static>>,
    attachments: Vec<ShaderProgram>,
}
struct ProgramStore {
    frags: Vec<FragmentShader>,
    verts: Vec<VertexShader>,
}

pub struct Manager__ {
    this: WeakManager,
    frags: HashMap<FragmentShader, ShaderStore>,
    verts: HashMap<VertexShader, ShaderStore>,
    programs: HashMap<ShaderProgram, ProgramStore>,
}
pub type Manager_ = RefCell<Manager__>;
pub type StrongManager = Rc<Manager_>;
pub type WeakManager = Weak<Manager_>;

pub type CompileOk = Program;
pub type CompileErr = proc(&Context3d) -> String;
pub type CompileResult = Result<CompileOk, CompileErr>;
pub type AttrBindSpec = Vec<(CString, TypedAttribute)>;
pub type UniformsSpec = Vec<(CString, UniformRole)>;
impl Manager__ {
    pub fn compile_simple(&mut self,
                          ctxt: &Context3d,
                          vert: &'static str,
                          frag: &'static str,
                          attrs: &AttrBindSpec,
                          uniforms: &UniformsSpec) -> CompileResult {
        use ppapi::gles::{CompilingVertexShader, CompilingFragmentShader};
        use super::{MatType, TwoByTwoMat, ThreeByThreeMat, FourByFourMat};

        let mut prog = ShaderProgram::new(ctxt);

        let vert_store = ShaderStore {
            src: vec!(Slice(vert)),
            attachments: vec!(unsafe { prog.get_program() }),
        };

        let vert_s: CompilingVertexShader   = CompileShader::new(ctxt, &vert_store.src);

        let frag_store = ShaderStore {
            src: vec!(Slice(frag)),
            attachments: vec!(unsafe { prog.get_program() }),
        };

        let frag_s: CompilingFragmentShader = CompileShader::new(ctxt, &frag_store.src);

        self.verts.insert(unsafe { vert_s.get_shader() }, vert_store);
        self.frags.insert(unsafe { frag_s.get_shader() }, frag_store);

        let vert_s = try!(vert_s.results(ctxt));
        let frag_s = try!(frag_s.results(ctxt));
        
        prog.attach_shader(ctxt, &vert_s);
        prog.attach_shader(ctxt, &frag_s);

        attrs.iter().fold(0, |index, &(ref name, ref attr)| {
            prog.bind_attrib_locale(ctxt, index, name);
            match attr.expose_type() {
                &MatType(TwoByTwoMat) => index + 2,
                &MatType(ThreeByThreeMat) => index + 3,
                &MatType(FourByFourMat) => index + 4,
                _ => index + 1,
            }
        });

        let linking = prog.link(ctxt);
        
        let prog_store = ProgramStore {
            verts: vec!(vert_s),
            frags: vec!(frag_s),
        };

        self.programs.insert(unsafe { linking.get_program() }, prog_store);
        linking.results(ctxt)
            .map(|mut linked| {
                let map: UniformBindings = FromIterator::from_iter
                    (uniforms.iter().map(|&(ref name, ref role)| {
                        let entry = BindingsEntry {
                            value: DirtyFlag::new(role.expose_type().to_typed_opt_value()),
                            index: linked.uniform_locale(ctxt, name),
                        };
                        (role.clone(), entry)
                    }));
                Program {
                    mgr: self.this.clone(),
                    prog: linked,
                    bindings: map,
                }
            })
    }
}
pub fn new_mgr() -> StrongManager {
    use std::mem;
    use std::ptr::write;
    let this = Rc::new(RefCell::new(unsafe { mem::uninitialized() }));
    let mgr = Manager__ {
        this: this.downgrade(),
        frags: HashMap::new(),
        verts: HashMap::new(),
        programs: HashMap::new(),
    };
    unsafe {
        write((*this).borrow_mut().deref_mut(), mgr);
    }
    this
}
