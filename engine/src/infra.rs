use ppapi;
use ppapi::Ticks;
use std::cell::Cell;
use ui;
use gl;
use animation;
use super::Engine;
use stages;
use std::cell::{RefCell, Ref, RefMut};
use std::default::Default;
use std::{cmp, ops, fmt, num};
use std::hash::Hash;
use std::hash::sip::SipState;
// infrastructure related thingies.

// Runtime data related to rendering.
pub struct RenditionData {
    pub instance: ppapi::Instance,
    pub context:  ppapi::Context3d,
    pub camera:   gl::Camera,

    pub mesh:     gl::mesh::Manager,
    pub shader:   gl::shader::StrongManager,
}
pub type ContextData = RenditionData;
impl RenditionData {
    pub fn new(engine: &mut Engine) -> RenditionData {
        RenditionData {
            instance: engine.instance.take_unwrap(),
            context:  engine.context.take_unwrap(),
            camera:   engine.camera.take_unwrap(),

            mesh:   engine.mesh.take_unwrap(),
            shader: engine.shader.clone(),
        }
    }
    pub fn return_data(self, engine: &mut Engine) {
        let RenditionData {
            instance: inst,
            context:  ctxt,
            camera:   cam,

            mesh:     mesh,
            shader:   _,
        } = self;

        engine.instance = Some(inst);
        engine.context  = Some(ctxt);
        engine.camera   = Some(cam);
        engine.mesh     = Some(mesh);
    }
}

#[deriving(Eq, Clone, Hash, Ord, PartialEq, PartialOrd)]
pub struct FrameId(pub uint);
impl ops::Deref<uint> for FrameId {
    fn deref<'a>(&'a self) -> &'a uint {
        let &FrameId(ref id) = self;
        id
    }
}
impl ops::Add<FrameId, FrameId> for FrameId {
    fn add(&self, rhs: &FrameId) -> FrameId {
        FrameId(**self + **rhs)
    }
}
impl ops::Sub<FrameId, FrameId> for FrameId {
    fn sub(&self, rhs: &FrameId) -> FrameId {
        FrameId(**self - **rhs)
    }
}
impl Default for FrameId {
    fn default() -> FrameId {
        FrameId(0)
    }
}
impl fmt::Show for FrameId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        (**self).fmt(f)
    }
}

pub struct Frame {
    pub id: FrameId,
    pub delta: Ticks,
    rendition_requests: Cell<uint>,
}
impl Frame {
    pub fn new(id: FrameId, delta: Ticks) -> Frame {
        Frame {
            id: id,
            delta: delta,
            rendition_requests: Cell::new(0),
        }
    }
    pub fn subframe(&self) -> Frame {
        Frame {
            id: self.id.clone(),
            delta: self.delta.clone(),
            rendition_requests: Cell::new(0),
        }
    }

    pub fn request_rendition(&self) {
        self.rendition_requests.set(self.rendition_requests.get() + 1)
    }
    pub fn rendition_requested(&self) -> bool {
        self.rendition_requests.get() != 0
    }
}

pub struct StageRenderingData {
    mesh: gl::mesh::Manager,
}

pub enum StageAnimationResult {
    NextStage(stages::Stage),
    
    Continue,
}
pub struct StageAnimationData {
    pub input: ui::input::StrongManager,
    pub cam:   gl::Camera,
}
impl StageAnimationData {
    pub fn new(engine: &mut super::Engine) -> StageAnimationData {
        use std::mem::{replace, uninitialized};
        StageAnimationData {
            input: replace(&mut engine.input,  unsafe { uninitialized() }),
            cam:   engine.camera.take_unwrap(),
        }
    }
    pub fn return_state(self, engine: &mut super::Engine) {
        use std::ptr::write;
        let StageAnimationData {
            input: input,
            cam:   cam,
        } = self;
        unsafe {
            write(&mut engine.input,  input);
        }
        engine.camera = Some(cam);
    }
}

pub trait Stage: animation::Animated<StageAnimationData, (StageAnimationData, 
                                                          StageAnimationResult)> {

    //pub fn initialize(&mut self) -> ~Initializer;

    fn view_changed(&mut self, _new_view: ppapi::View) {}
    
    fn render(&self, ctxt: &ContextData, camera: &gl::Camera);

    //pub fn deinitialize(&mut self) -> ~Deinitializer;
    
}

#[deriving(Eq, Clone, Hash, PartialEq, Ord, PartialOrd, Encodable, Decodable, Show)]
pub struct Key(pub uint);

impl ops::Add<uint, Key> for Key {
    fn add(&self, rhs: &uint) -> Key {
        Key(**self + *rhs)
    }
}
impl ops::Sub<uint, Key> for Key {
    fn sub(&self, rhs: &uint) -> Key {
        Key(**self - *rhs)
    }
}

impl ops::Deref<uint> for Key {
    fn deref<'a>(&'a self) -> &'a uint {
        let &Key(ref inner) = self;
        inner
    }
}
impl num::Bounded for Key {
    fn min_value() -> Key {
        Key(num::Bounded::min_value())
    }
    fn max_value() -> Key {
        Key(num::Bounded::max_value())
    }
}
impl cmp::Equiv<uint> for Key {
    fn equiv(&self, rhs: &uint) -> bool {
        **self == *rhs
    }
}

#[deriving(Clone)]
pub struct DirtyCount(Cell<uint>);
impl cmp::Eq for DirtyCount { }
impl cmp::PartialEq for DirtyCount {
    fn eq(&self, rhs: &DirtyCount) -> bool {
        self.inner().get() == rhs.inner().get()
    }
}
impl cmp::PartialOrd for DirtyCount {
    fn partial_cmp(&self, rhs: &DirtyCount) -> Option<cmp::Ordering> {
        Some(self.cmp(rhs))
    }
}
impl cmp::Ord for DirtyCount {
    fn cmp(&self, rhs: &DirtyCount) -> cmp::Ordering {
        self.inner().get().cmp(&rhs.inner().get())
    }
}
impl Hash for DirtyCount {
    fn hash(&self, state: &mut SipState) {
        self.inner().get().hash(state)
    }
}

impl DirtyCount {
    pub fn new_dirty() -> DirtyCount {
        DirtyCount(Cell::new(1))
    }
    fn inner<'a>(&'a self) -> &'a Cell<uint> {
        let &DirtyCount(ref inner) = self;
        inner
    }
    pub fn bump(&self) {
        let inner = self.inner();
        let count = inner.get() + 1;
        inner.set(count);
    }
    pub fn reset(&self) -> uint {
        let count = self.inner().get();
        self.inner().set(0);
        count
    }
    pub fn dirty(&self) -> bool {
        self.inner().get() != 0
    }
}
impl CacheChecker<uint> for DirtyCount {
    fn is_fresh(&self, key: &uint) -> bool {
        !(self.inner().get() > *key)
    }
    fn fresh_key(&self) -> uint {
        self.inner().get()
    }
}
impl Default for DirtyCount {
    fn default() -> DirtyCount {
        DirtyCount(Cell::new(0))
    }
}

#[deriving(Clone)]
pub struct DirtyFlag<T> {
    flag: Cell<bool>,
    value: T,
}
impl<T: Default> Default for DirtyFlag<T> {
    fn default() -> DirtyFlag<T> {
        DirtyFlag {
            flag: Cell::new(false),
            value: Default::default(),
        }
    }
}
impl<T> DirtyFlag<T> {
    // creates a counter already in the dirty state.
    pub fn new(v: T) -> DirtyFlag<T> {
        DirtyFlag {
            flag: Cell::new(true),
            value: v,
        }
    }

    pub fn set(&mut self, v: T) {
        self.flag.set(true);
        self.value = v;
    }
    pub fn get_ref<'a>(&'a self) -> &'a T {
        &self.value
    }
    pub fn is_dirty(&self) -> bool {
        self.flag.get()
    }
    pub fn reset<'a>(&'a self) -> Option<&'a T> {
        if self.is_dirty() {
            self.flag.set(false);
            Some(self.get_ref())
        } else {
            None
        }
    }
}
impl<T: PartialEq> DirtyFlag<T> {
    // If the value provided is equal to the value in the store, don't dirty self.
    pub fn maybe_set(&mut self, v: T) {
        if !self.get_ref().eq(&v) {
            self.set(v)
        }
    }
}

#[deriving(Clone)]
pub struct InternalCache<TKey, TCache>(RefCell<Option<TKey>>, RefCell<Option<TCache>>);
impl<TKey: Eq, TCache: Eq> Eq for InternalCache<TKey, TCache> { }
impl<TKey: PartialEq, TCache: PartialEq> PartialEq for InternalCache<TKey, TCache> {
    fn eq(&self, rhs: &InternalCache<TKey, TCache>) -> bool {
        let (sl, sr) = self.inner();
        let (rl, rr) = rhs.inner();
        (*sl.borrow()) == (*rl.borrow()) &&
            (*sr.borrow()) == (*rr.borrow())
    }
}

pub struct InternalCacheValRef<'a, T>(Ref<'a, T>);
impl<'a, T> InternalCacheValRef<'a, Option<T>> {
    pub fn with<U>(&self, f: |Option<&T>| -> U) -> U {
        let &InternalCacheValRef(ref r) = self;
        f((*r).as_ref())
    }
}

impl<'a, T> Deref<T> for InternalCacheValRef<'a, Option<T>> {
    fn deref<'b>(&'b self) -> &'b T {
        let &InternalCacheValRef(ref r) = self;
        (*r).get_ref()
    }
}

pub trait CacheChecker<TKey> {
    fn is_fresh(&self, key: &TKey) -> bool;
    fn fresh_key(&self) -> TKey;
}
impl<TKey, TCache> InternalCache<TKey, TCache> {
    pub fn new_fresh<T: CacheChecker<TKey>>(key: &T, val: TCache) -> InternalCache<TKey, TCache> {
        InternalCache(RefCell::new(Some(key.fresh_key())), RefCell::new(Some(val)))
    }
    pub fn new_stale() -> InternalCache<TKey, TCache> {
        InternalCache(RefCell::new(None), RefCell::new(None))
    }
    fn inner<'a>(&'a self) -> (&'a RefCell<Option<TKey>>, &'a RefCell<Option<TCache>>) {
        let &InternalCache(ref key, ref val) = self;
        (key, val)
    }
    fn borrow_key<'a>(&'a self) -> RefMut<'a, Option<TKey>> {
        let (cell, _) = self.inner();
        cell.borrow_mut()
    }
    fn borrow_value<'a>(&'a self) -> RefMut<'a, Option<TCache>> {
        let (_, cell) = self.inner();
        cell.borrow_mut()
    }
    pub fn is_fresh<T: CacheChecker<TKey>>(&self, checker: &T) -> bool {
        let key = self.borrow_key();
        if (*self.borrow_value()).is_none() {
            return false;
        }
        match *key {
            Some(ref key) => checker.is_fresh(key),
            None => false,
        }
    }
    fn update_key<T: CacheChecker<TKey>>(&self, checker: &T) {
        *self.borrow_key() = Some(checker.fresh_key());
    }
    pub fn get<'a, T: CacheChecker<TKey>>(&'a self,
                                          checker: &T,
                                          f: |Option<TCache>| -> TCache) -> InternalCacheValRef<'a, Option<TCache>> {
        let is_fresh = self.is_fresh(checker);
        if !is_fresh {
            self.update_key(checker);
            let mut borrow = self.borrow_value();
            *borrow = Some(f((*borrow).take()));
        }

        let (_, cell) = self.inner();
        InternalCacheValRef(cell.borrow())
    }
    // get the possibly stale value.
    pub fn get_stale<'a>(&'a self) -> InternalCacheValRef<'a, Option<TCache>> {
        let (_, cell) = self.inner();
        InternalCacheValRef(cell.borrow())
    }
}
impl<TKey: Clone, TCache: Clone> InternalCache<TKey, TCache> {
    pub fn get_opt<'a, T: CacheChecker<TKey>>(&'a self,
                                              checker: &T,
                                              f: |Option<TCache>| -> Option<TCache>) -> Option<TCache> {
        if self.is_fresh(checker) {
            self.update_key(checker);
            let mut borrow = self.borrow_value();
            *borrow = f((*borrow).take());
        }
        let (_, cell) = self.inner();
        (*cell.borrow()).clone()
    }
}
