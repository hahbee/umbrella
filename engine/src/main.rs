
//! This is the main 'engine' for every Hahbee app on any platform.

#![crate_name = "hahbee"]
#![crate_type = "bin"]
#![feature(globs)]
#![feature(macro_rules)]
#![feature(simd)]
#![feature(phase)]
#![feature(default_type_params, struct_variant)]
#![feature(unsafe_destructor)]
//#![warn(missing_doc)]
#![deny(uppercase_variables)]
#![no_main]

extern crate std;
extern crate native;
#[phase(plugin, link)]
extern crate log;
extern crate collections;
extern crate sync;
extern crate rand;
extern crate serialize;
extern crate http;
extern crate url;
extern crate libc;
extern crate ppapi;

use ppapi::{Ticks, InstanceCallbacks, Instance};
use ppapi::ppb;
use std::default::Default;
use std::collections::hashmap::HashMap;
use infra::{Frame, FrameId, Continue, NextStage,
            StageAnimationData, StageAnimationResult, RenditionData};
use stages::Stage;
use animation::Animated;

use stages::landing;

pub mod math;
pub mod gl;
pub mod star_field;
pub mod objs;
pub mod infra;
pub mod ui;
pub mod animation;
pub mod stages;

// Because our PNaCl module will likely be fairly large, we'll vet the OpenGL device in JS
// and then if not passing display a sad face or what-not.

#[no_mangle]
#[allow(dead_code)]
#[cfg(target_os = "nacl")]
pub extern fn ppapi_instance_created(instance: ppapi::Instance,
                                 _args: || -> HashMap<String, String>) -> Box<InstanceCallbacks> {
    error!("Instance created");

    fn create_landing(ctxt: &mut RenditionData, _view: ppapi::View) -> Stage {
        use stages::LandingStage;
        LandingStage(landing::Landing::new(ctxt))
    }

    ppapi::mount("/static", "/http", "http", "").expect("couldn't mount static assets");

    // TODO
    instance.request_input_events
        (ppapi::ffi::PP_INPUTEVENT_CLASS_MOUSE).expect("mouse event request failed!");
    instance.request_input_events
        (ppapi::ffi::PP_INPUTEVENT_CLASS_KEYBOARD).expect("keyboard event request failed!");
    instance.request_input_events
        (ppapi::ffi::PP_INPUTEVENT_CLASS_WHEEL).expect("mouse wheel event request failed!");

    let engine = Engine::new(instance, stages::UninitializedStage(create_landing));
    box engine as Box<InstanceCallbacks>
}
static DELTA_LEN: u64 = 4;
pub struct Engine {
    instance: Option<Instance>,
    context: Option<ppapi::Context3d>,
    stage: Stage,

    camera: Option<gl::Camera>,
    view:   Option<ppapi::View>,

    shader: gl::shader::StrongManager,
    input:  ui::input::StrongManager,
    mesh:   Option<gl::mesh::Manager>,

    has_focus: bool,

    frame: FrameId,
    timed_frame: u64,
    ignored_frames: u64,

    session_avg_tpf: Ticks,
    total_frame_time: Ticks,
    wall_time: Ticks,
    last_frame_ticks: Ticks,
    
    real_delta: Ticks,
    smoothed_delta: Ticks,
    deltas: [Ticks, ..DELTA_LEN],
}
static IDEAL_TICKS_PER_FRAME: Ticks = 1.0 / 60.0;
impl Engine {
    pub fn new(instance: Instance, initial_stage: Stage) -> Engine {
        Engine {
            instance: Some(instance),
            context: Default::default(),
            stage: initial_stage,

            shader: gl::shader::new_mgr(),
            input:  ui::input::new_mgr(),
            mesh:   Default::default(),

            camera: Default::default(),
            view:   Default::default(),

            has_focus: true,
            
            frame: Default::default(),
            timed_frame: 0,
            ignored_frames: 0,
            
            session_avg_tpf: 0.0,
            total_frame_time: 0.0,
            wall_time: 0.0,
            last_frame_ticks: ppb::get_core().get_time_ticks(),
            
            real_delta: 0.0,
            smoothed_delta: 0.0,
            deltas: [IDEAL_TICKS_PER_FRAME, ..(DELTA_LEN as uint)],
        }
    }

    pub fn initialize(&mut self, view: ppapi::View) {
        let vp = view.rect().unwrap();
        self.view = Some(view);
        self.context = Some({
            let attribs = vec!((ppapi::ffi::PP_GRAPHICS3DATTRIB_WIDTH as i32, vp.size.width),
                               (ppapi::ffi::PP_GRAPHICS3DATTRIB_HEIGHT as i32, vp.size.height));
            
            // TODO
            let context = self.instance.get_ref().create_3d_context(None, attribs).unwrap();
            // TODO
            self.instance.get_ref().bind_context(&context).expect("this should not fail");
            context
        });
        self.camera = Some({
            let aspect = (vp.size.width as f32) / (vp.size.height as f32);
            gl::Camera::new(aspect,
                            2000.0f32,
                            0.01f32)
        });
        self.swap_buffers();
    }

    pub fn begin_render(&mut self) -> Frame {
        Frame::new(self.frame, self.smoothed_delta)
    }
    pub fn finish_render(&mut self, prev: Frame) {
        let ticks = ppb::get_core().get_time_ticks();
        self.real_delta = ticks - self.last_frame_ticks;
        self.last_frame_ticks = ticks;

        if self.real_delta > 0.1 {
            warn!("Frame \\#{} too slow: {}", self.frame, self.real_delta);
        } else if prev.rendition_requested() {
            // Only count this frame if we actually rendered. Otherwise,
            // while only animating, we'll have really short frames, giving
            // us false times.

            self.timed_frame += 1;
            self.deltas[(self.timed_frame % DELTA_LEN) as uint] = self.real_delta;
            self.smoothed_delta = self.deltas
                .iter()
                .fold(0.0f64, |a, &v| a + v ) / DELTA_LEN as f64;
        }

        self.total_frame_time += self.smoothed_delta;
        self.wall_time += self.real_delta;
        self.frame = self.frame + FrameId(1);
    }
    pub fn render(&mut self) {
        let frame = self.begin_render();

        let stage_animation = StageAnimationData::new(self);

        let (stage_animation, result) = self.stage.animate(&frame, stage_animation);
        stage_animation.return_state(self);
        match result {
            Continue if frame.rendition_requested() => {
                let mut rend_data = RenditionData::new(self);
                self.stage.render(&mut rend_data);
                rend_data.context.swap_buffers();
                rend_data.return_data(self);
            }
            Continue => {}
            NextStage(next_stage) => {
                debug!("stage change");
                self.stage = next_stage;
            }
        };
        
        self.finish_render(frame);
    }
    pub fn swap_buffers(&self) {
        self.context.get_ref().swap_buffers()
    }
}

impl ppapi::InstanceCallbacks for Engine {
    fn on_destroy(&mut self) {}

    fn on_buffers_swapped(&mut self, prev_code: ppapi::Code) {
        if !prev_code.is_ok() {
            error!("the previous frame had an error: `{}`", prev_code);
        }

        self.render();
        self.swap_buffers();
    }

    fn on_change_view(&mut self, view: ppapi::View) {
        if self.stage.is_uninitialized() {
            self.initialize(view);
        } else {
            self.stage.view_changed(view);
        }
    }
    fn on_change_focus(&mut self, has_focus: bool) {
        self.has_focus = has_focus;
    }
    fn on_message(&mut self, _message: ppapi::AnyVar) {
        // TODO 
    }
    fn on_kb_input(&mut self, event: ppapi::KeyboardInputEvent) -> bool {
        use ppapi::input::Class;
        (*self.input).borrow_mut().on_input_event(Class::new(event));
        true
    }
    fn on_mouse_input(&mut self, event: ppapi::MouseInputEvent) -> bool {
        use ppapi::input::Class;
        (*self.input).borrow_mut().on_input_event(Class::new(event));
        true
    }
    fn on_wheel_input(&mut self, event: ppapi::WheelInputEvent) -> bool {
        use ppapi::input::Class;
        (*self.input).borrow_mut().on_input_event(Class::new(event));
        true
    }
    fn on_touch_input(&mut self, event: ppapi::TouchInputEvent) -> bool {
        use ppapi::input::Class;
        (*self.input).borrow_mut().on_input_event(Class::new(event));
        true            
    }
    fn on_ime_input(&mut self,   event: ppapi::IMEInputEvent)   -> bool {
        use ppapi::input::Class;
        (*self.input).borrow_mut().on_input_event(Class::new(event));
        true
    }
    fn on_graphics_context_lost(&mut self) {
        unimplemented!()
    }
}

pub struct Main {
    bee: objs::Bee,
    
    star_field: star_field::StarField,
}
impl infra::Stage for Main {
    fn render(&self,
              _ctxt: &infra::ContextData,
              _camera: &gl::Camera) {
        
    }
}
impl animation::Animated<StageAnimationData, (StageAnimationData, 
                                              StageAnimationResult)> for Main {
    fn animate(&mut self,
               _frame: &Frame,
               ctxt: StageAnimationData) -> (StageAnimationData,
                                             StageAnimationResult) {
        
        (ctxt, infra::Continue)
    }
}
