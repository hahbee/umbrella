use super::{Orientation, Position};
use super::vector::{Vec4, Vec3, Vector4, Vector3, Vector2, Vector};
use super::quaternion;

use std::{cmp, clone};
use std::ops::{Index, IndexMut};
use serialize;

pub struct Matrix4x4 {
    cols: [Vec4, ..4],
}

impl Matrix4x4 {
    pub fn new_from_cols(col0: Vec4, col1: Vec4, col2: Vec4, col3: Vec4) -> Matrix4x4 {
        Matrix4x4 {
            cols: [col0, col1, col2, col3],
        }
    }

    #[inline]
    pub fn identity() -> Matrix4x4 {
        Matrix4x4 {
            cols: [Vector2::unit_x(),
                   Vector2::unit_y(),
                   Vector3::unit_z(),
                   Vector4::unit_w()],
        }
    }

    pub fn as_slice<'a>(&'a self) -> &'a [f32] {
        use std::slice::raw::buf_as_slice;
        use std::mem::transmute;
        unsafe {
            let ptr: *const f32 = transmute(self.cols.as_ptr());
            buf_as_slice(ptr,
                         16,
                         |b| transmute(b) )
        }
    }

    #[inline]
    pub fn get_entry(&self, col: uint, row: uint) -> f32 {
        self[col][row]
    }
    #[inline]
    pub fn set_entry(&mut self, col: uint, row: uint, val: f32) {
        let colv = self.get_col(col);
        self.set_col(col, colv.with(row, val));
    }
    
    #[inline]
    pub unsafe fn unsafe_get_entry(&self, col: uint, row: uint) -> f32 {
        self.unsafe_get_col(col)[row]
    }
    #[inline]
    pub unsafe fn unsafe_set_entry(&mut self, col: uint, row: uint, val: f32) {
        let colv = self.unsafe_get_col(col);
        self.unsafe_set_col(col, colv.with(row, val));
    }

    #[inline]
    pub fn get_col(&self, idx: uint) -> Vec4 {
        self.cols[idx]
    }
    #[inline]
    pub fn set_col(&mut self, idx: uint, v: Vec4) {
        self.cols[idx] = v;
    }

    #[inline]
    pub unsafe fn unsafe_get_col(&self, idx: uint) -> Vec4 {
        (*self.cols.unsafe_ref(idx)).clone()
    }
    #[inline]
    pub unsafe fn unsafe_set_col(&mut self, idx: uint, v: Vec4) {
        self.cols.unsafe_set(idx, v);
    }

    #[inline]
    pub fn new_projection(aspect: f32, near: f32, far: f32) -> Matrix4x4 {
        let right = near * aspect;

        let right_minus_left = 2.0 * right;
        let far_minus_near   = far - near;

        let mut projection = Matrix4x4::identity();
        unsafe {
            projection.unsafe_set_entry(0, 0, (2.0 * near) / right_minus_left);
            projection.unsafe_set_entry(1, 1, (2.0 * near) / right_minus_left);
            projection.unsafe_set_entry(2, 2, -(far + near) / (far_minus_near));
            projection.unsafe_set_entry(3, 3, (-2.0 * far * near) / (far_minus_near));
        }
        return projection;
    }
}
impl Index<uint, Vec4> for Matrix4x4 {
    fn index<'a>(&'a self, idx: &uint) -> &'a Vec4 {
        &self.cols[*idx]
    }
}
impl<'a> Index<uint, Vec4> for &'a Matrix4x4 {
    fn index<'b>(&'b self, idx: &uint) -> &'b Vec4 {
        &self.cols[*idx]
    }
}
impl IndexMut<uint, Vec4> for Matrix4x4 {
    fn index_mut<'a>(&'a mut self, idx: &uint) -> &'a mut Vec4 {
        &mut self.cols[*idx]
    }
}
impl<'a> IndexMut<uint, Vec4> for &'a mut Matrix4x4 {
    fn index_mut<'b>(&'b mut self, idx: &uint) -> &'b mut Vec4 {
        &mut self.cols[*idx]
    }
}
impl clone::Clone for Matrix4x4 {
    fn clone(&self) -> Matrix4x4 {
        unsafe {
            Matrix4x4 {
                cols: [self.unsafe_get_col(0),
                       self.unsafe_get_col(1),
                       self.unsafe_get_col(2),
                       self.unsafe_get_col(3)]
            }
        }
    }
}
impl cmp::PartialEq for Matrix4x4 {
    fn eq(&self, rhs: &Matrix4x4) -> bool {
        self.cols[0] == rhs.cols[0] &&
            self.cols[1] == rhs.cols[1] &&
            self.cols[2] == rhs.cols[2] &&
            self.cols[3] == rhs.cols[3]
    }
}

impl super::Orientation for Matrix4x4 {
    #[inline]
    #[allow(unnecessary_parens)]
    fn rot_invert(&mut self) {
        let m00 = (*self)[0][0];
        let m01 = (*self)[0][1];
        let m02 = (*self)[0][2];
        let m03 = (*self)[0][3];

        let m10 = (*self)[1][0];
        let m11 = (*self)[1][1];
        let m12 = (*self)[1][2];
        let m13 = (*self)[1][3];

        let m20 = (*self)[2][0];
        let m21 = (*self)[2][1];
        let m22 = (*self)[2][2];
        let m23 = (*self)[2][3];

        let m30 = (*self)[3][0];
        let m31 = (*self)[3][1];
        let m32 = (*self)[3][2];
        let m33 = (*self)[3][3];

        let mut v0 = m20 * m31 - m21 * m30;
        let mut v1 = m20 * m32 - m22 * m30;
        let mut v2 = m20 * m33 - m23 * m30;
        let mut v3 = m21 * m32 - m22 * m31;
        let mut v4 = m21 * m33 - m23 * m31;
        let mut v5 = m22 * m33 - m23 * m32;

        let t00 =   (v5 * m11 - v4 * m12 + v3 * m13);
        let t10 = - (v5 * m10 - v2 * m12 + v1 * m13);
        let t20 =   (v4 * m10 - v2 * m11 + v0 * m13);
        let t30 = - (v3 * m10 - v1 * m11 + v0 * m12);

        let invDet = 1.0f32 / (t00 * m00 + t10 * m01 + t20 * m02 + t30 * m03);

        let d00 = t00 * invDet;
        let d10 = t10 * invDet;
        let d20 = t20 * invDet;
        let d30 = t30 * invDet;
        let d0: Vec4 = Vector::wrap([d00, d10, d20, d30]);

        let d01 = - (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
        let d11 =   (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
        let d21 = - (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
        let d31 =   (v3 * m00 - v1 * m01 + v0 * m02) * invDet;
        let d1: Vec4 = Vector::wrap([d01, d11, d21, d31]);

        v0 = m10 * m31 - m11 * m30;
        v1 = m10 * m32 - m12 * m30;
        v2 = m10 * m33 - m13 * m30;
        v3 = m11 * m32 - m12 * m31;
        v4 = m11 * m33 - m13 * m31;
        v5 = m12 * m33 - m13 * m32;

        let d02 =   (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
        let d12 = - (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
        let d22 =   (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
        let d32 = - (v3 * m00 - v1 * m01 + v0 * m02) * invDet;
        let d2: Vec4 = Vector::wrap([d02, d12, d22, d32]);

        v0 = m21 * m10 - m20 * m11;
        v1 = m22 * m10 - m20 * m12;
        v2 = m23 * m10 - m20 * m13;
        v3 = m22 * m11 - m21 * m12;
        v4 = m23 * m11 - m21 * m13;
        v5 = m23 * m12 - m22 * m13;

        let d03 = - (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
        let d13 =   (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
        let d23 = - (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
        let d33 =   (v3 * m00 - v1 * m01 + v0 * m02) * invDet;
        let d3: Vec4 = Vector::wrap([d03, d13, d23, d33]);

        unsafe {
            self.unsafe_set_col(0, d0);
            self.unsafe_set_col(1, d1);
            self.unsafe_set_col(2, d2);
            self.unsafe_set_col(3, d3);
        }
    }

    fn rot_unit_invert(&mut self) {
        self.rot_invert();
    }
    
    fn rotate_position_inplace<T: super::Position>(&self, _v: &mut T) {
        unimplemented!()
    }

    fn set_mut_rot_from_quat(&mut self, q: quaternion::Quaternion) {
        let x = q[0];
        let y = q[1];
        let z = q[2];
        let w = q[3];

        let fTx  = x+x;
        let fTy  = y+y;
        let fTz  = z+z;
        let fTwx = fTx*w;
        let fTwy = fTy*w;
        let fTwz = fTz*w;
        let fTxx = fTx*x;
        let fTxy = fTy*x;
        let fTxz = fTz*x;
        let fTyy = fTy*y;
        let fTyz = fTz*y;
        let fTzz = fTz*z;

        let (zero, one, two) = unsafe {
            let zero = self.unsafe_get_col(0);
            let one  = self.unsafe_get_col(1);
            let two  = self.unsafe_get_col(2);
            (zero, one, two)
        };
        unsafe {
            self.unsafe_set_col(0, Vector::wrap([1.0f32-(fTyy+fTzz),
                                                 fTxy-fTwz,
                                                 fTxz+fTwy,
                                                 zero[3]]));
            self.unsafe_set_col(1, Vector::wrap([fTxy+fTwz,
                                                 1.0f32-(fTxx+fTzz),
                                                 fTyz-fTwx,
                                                 one[3]]));
            self.unsafe_set_col(2, Vector::wrap([fTxz-fTwy,
                                                 fTyz+fTwx,
                                                 1.0f32-(fTxx+fTyy),
                                                 two[3]]));
        }
    }
    fn set_mut_rot_from_mat(&mut self, m: &Matrix4x4) {
        unsafe {
            self.set_mut_rot_from_axes
                (m.unsafe_get_col(0),
                 m.unsafe_get_col(1),
                 m.unsafe_get_col(2))
        }
    }
    fn set_mut_rot_from_axes<X: Vector3, Y: Vector3, Z: Vector3>(&mut self, x: X, y: Y, z: Z) {
        let (zero, one, two) = unsafe {
            let zero = self.unsafe_get_col(0);
            let one  = self.unsafe_get_col(1);
            let two  = self.unsafe_get_col(2);
            (zero, one, two)
        };
        unsafe {
            self.unsafe_set_col(0, Vector::wrap([x[0],
                                                 x[1],
                                                 x[2],
                                                 zero[3]]));
            self.unsafe_set_col(1, Vector::wrap([y[0],
                                                 y[1],
                                                 y[2],
                                                 one[3]]));
            self.unsafe_set_col(2, Vector::wrap([z[0],
                                                 z[1],
                                                 z[2],
                                                 two[3]]));
        }
    }
}
impl super::Position for Matrix4x4 {
    fn get_position(&self) -> Vec3 {
        unsafe {
            self.unsafe_get_col(3).as_imm_vec3().clone()
        }
    }
    fn set_position<T: super::Position>(&mut self, v: &T) {
        let v = v.get_position();
        let col3 = unsafe { self.unsafe_get_col(3) };
        unsafe {
            self.unsafe_set_col(3, Vector::wrap([v[0],
                                                 v[1],
                                                 v[2],
                                                 col3[3]]));
        }
    }
}
impl super::Pose for Matrix4x4 {}

impl<E, D: serialize::Decoder<E>> serialize::Decodable<D, E> for Matrix4x4 {
    fn decode(d: &mut D) -> Result<Matrix4x4, E> {
        let col1 = try!(serialize::Decodable::decode(d));
        let col2 = try!(serialize::Decodable::decode(d));
        let col3 = try!(serialize::Decodable::decode(d));
        let col4 = try!(serialize::Decodable::decode(d));
        Ok(Matrix4x4::new_from_cols(col1, col2, col3, col4))
    }
}
impl<E, S: serialize::Encoder<E>> serialize::Encodable<S, E> for Matrix4x4 {
    fn encode(&self, e: &mut S) -> Result<(), E> {
        try!(self.cols[0].encode(e));
        try!(self.cols[1].encode(e));
        try!(self.cols[2].encode(e));
        try!(self.cols[3].encode(e));
        Ok(())
    }
}
