use std::clone;

pub use math::matrix::Matrix4x4;
pub use math::quaternion::Quaternion;
pub use math::vector::{Vector2, Vector3, Vector4,
                       Vec2, Vec3, Vec4, Vector, CrossProduct};

pub mod matrix;
pub mod quaternion;
pub mod vector;

pub trait Orientation: clone::Clone {
    #[inline]
    fn rot_inverse(&self) -> Self {
        let mut s = self.clone();
        s.rot_invert();
        s
    }
    fn rot_invert(&mut self);

    #[inline]
    fn rot_unit_inverse(&self) -> Self {
        let mut s = self.clone();
        s.rot_unit_invert();
        s
    }
    fn rot_unit_invert(&mut self);
    
    #[inline]
    fn rotate_position<T: Position>(&self, v: &T) -> T {
        let mut v = v.clone();
        self.rotate_position_inplace(&mut v);
        v
    }
    fn rotate_position_inplace<T: Position>(&self, v: &mut T);

    fn set_mut_rot_from_quat(&mut self, q: quaternion::Quaternion);
    fn set_mut_rot_from_mat(&mut self, m: &Matrix4x4) {
        unsafe {
            self.set_mut_rot_from_axes
                (m.unsafe_get_col(0),
                 m.unsafe_get_col(1),
                 m.unsafe_get_col(2))
        }
    }
    fn set_mut_rot_from_axes<X: Vector3, Y: Vector3, Z: Vector3>
        (&mut self, x: X, y: Y, z: Z);

    #[inline]
    fn set_imm_rot_from_quat(&self, q: quaternion::Quaternion) -> Self {
        let mut clone = self.clone();
        clone.set_mut_rot_from_quat(q);
        return clone;
    }
    #[inline]
    fn set_imm_rot_from_mat(&self, m: &matrix::Matrix4x4) -> Self {
        let mut clone = self.clone();
        clone.set_mut_rot_from_mat(m);
        return clone;
    }
    #[inline]
    fn set_imm_rot_from_axes<X: Vector3, Y: Vector3, Z: Vector3>
        (&self, x: X, y: Y, z: Z) -> Self {
        let mut clone = self.clone();
        clone.set_mut_rot_from_axes(x, y, z);
        return clone;
    }
}

pub trait Position: clone::Clone {
    fn get_position(&self) -> Vec3;
    fn set_position<T: Position>(&mut self, v: &T);
}

pub trait Pose: Position + Orientation + clone::Clone {
    #[inline]
    fn move_relative_imm<T: Position>(&self, pos: &T) -> Self {
        let mut s = self.clone();
        s.move_relative_mut(pos);
        s
    }
    #[inline]
    fn move_relative_mut<T: Position>(&mut self, pos: &T) {
        let new_pos = self.get_position() + self.rotate_position(&pos.get_position());
        self.set_position(&new_pos);
    }

    #[inline]
    fn look_at_imm<T: Position>(&self, target: &T, up: Vec3) -> Self {
        let mut s = self.clone();
        let pos = target.get_position();
        s.look_at_mut(&pos, &up);
        s
    }
    #[inline]
    fn look_at_mut<T: Position, U: Position>(&mut self, target: &T, up: &U) {
        let pos = self.get_position();
        let target = target.get_position();
        if pos == target {
            return;
        }
        let up = up.get_position();

        let (_, z) = target.sub_imm(pos).normalized();
        let (_, x) = up.cross_product_imm(z).normalized();
        let (_, y) = z.cross_product_imm(x).normalized();
        self.set_mut_rot_from_axes(x, y, z);
    }
}

