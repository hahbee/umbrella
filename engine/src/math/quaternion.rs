use super::vector::{Vector3, Vector};
use super::Position;

use std::ops;
use std::mem;
use std::{clone, default};

pub struct Quaternion([f32, ..4]);

//static UNIT:     Quaternion = Quaternion(gather_simd!(0.0f32, 0.0f32, 0.0f32,  1.0f32));
//static NEG_UNIT: Quaternion = Quaternion(gather_simd!(0.0f32, 0.0f32, 0.0f32, -1.0f32));

impl super::Orientation for Quaternion {
    #[inline]
    fn rot_invert(&mut self) {
        
    }
    #[inline]
    fn rot_unit_invert(&mut self) {
    }

    #[inline]
    fn rot_unit_inverse(&self) -> Quaternion {
        self.unit_inverse()
    }
    
    #[inline]
    fn rotate_position_inplace<T: Position>(&self, _v: &mut T) {
        // nVidia SDK implementation
        unimplemented!()
    }

    #[inline]
    fn set_mut_rot_from_quat(&mut self, quat: Quaternion) {
        *self = quat;
    }
    #[inline]
    fn set_mut_rot_from_axes<X: Vector3, Y: Vector3, Z: Vector3>
        (&mut self, x_axes: X, y_axes: Y, z_axes: Z) {
        // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
        // article "Quaternion Calculus and Fast Animation".
        
        let trace = x_axes[0] + y_axes[1] + z_axes[2];
        if trace > 0.0f32 {
            let root = (trace + 1.0f32).sqrt();
            let w = 0.5f32 * root;
            let root = 0.5f32 / root;
            let x = (z_axes[1] - y_axes[2]) * root;
            let y = (x_axes[2] - z_axes[0]) * root;
            let z = (y_axes[0] - x_axes[1]) * root;
            *self = Quaternion::from_values(x, y, z, w);
        } else {
            let next = [1, 2, 0];
            let columns = [x_axes.as_imm_vec3(),
                           y_axes.as_imm_vec3(),
                           z_axes.as_imm_vec3()];

            let mut i = 0;

            if y_axes[1] > x_axes[0] {
                i = 1;
            }
            if z_axes[2] > columns[i][i] {
                i = 2;
            }

            let j = next[i];
            let k = next[j];

            let root_sqd = columns[i][i] - columns[j][j] - columns[k][k] + 1.0f32;
            let root = root_sqd.sqrt();
            
            let x = 0.5f32 * root;
            let root = 0.5f32 / root;
            let y = (columns[k][j] - columns[j][k]) * root;
            let z = (columns[j][i] - columns[i][j]) * root;
            let w = (columns[k][i] - columns[i][k]) * root;
            
            *self = Quaternion::from_values(x, y, z, w);
        }
    }
}

impl Quaternion {
    pub fn identity() -> Quaternion {
        Quaternion::from_values(0.0f32, 0.0f32, 0.0f32, 1.0f32)
    }
    pub fn from_array(a: [f32, ..4]) -> Quaternion {
        Quaternion(a)
    }

    pub fn get_ref<'a>(&'a self) -> &'a [f32, ..4] {
        let &Quaternion(ref inner) = self;
        inner
    }
    pub fn get_mut_ref<'a>(&'a mut self) -> &'a mut [f32, ..4] {
        let &Quaternion(ref mut inner) = self;
        inner
    }

    pub fn unwrap(self) -> [f32, ..4] {
        let Quaternion(inner) = self;
        inner
    }
    pub fn from_values(x: f32, y: f32, z: f32, w: f32) -> Quaternion {
        Quaternion([x, y, z, w])
    }
    pub fn new_unit() -> Quaternion {
        Quaternion::from_values(0.0f32, 0.0f32, 0.0f32, 1.0f32)
    }
    fn _unit_inverse(&self) -> [f32, ..4] {
        use std::iter;
        let mut dest = [-1.0f32, ..4];
        for i in iter::range(0, 4) {
            dest[i] = dest[i] * self[i];
        }
        dest
    }
    pub fn unit_inverse(&self) -> Quaternion {
        Quaternion::from_array(self._unit_inverse())
    }
}

impl ops::Mul<Quaternion, Quaternion> for Quaternion {
    fn mul(&self, rhs: &Quaternion) -> Quaternion {
        let (x1, y1, z1, w1) = (self[0], self[1], self[2], self[3]);
        let (x2, y2, z2, w2) = (rhs[0],  rhs[1],  rhs[2],  rhs[3]);
        Quaternion::from_values(w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2,
                                w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2,
                                w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2,
                                w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2)
    }
}
impl clone::Clone for Quaternion {
    fn clone(&self) -> Quaternion {
        unsafe { mem::transmute_copy(self) }
    }
}
impl default::Default for Quaternion {
    fn default() -> Quaternion {
        Quaternion::identity()
    }
}
impl Index<uint, f32> for Quaternion {
    fn index<'a>(&'a self, idx: &uint) -> &'a f32 {
        &self.get_ref()[*idx]
    }
}
impl<'a> Index<uint, f32> for &'a Quaternion {
    fn index<'b>(&'b self, idx: &uint) -> &'b f32 {
        &self.get_ref()[*idx]
    }
}
impl IndexMut<uint, f32> for Quaternion {
    fn index_mut<'a>(&'a mut self, idx: &uint) -> &'a mut f32 {
        &mut self.get_mut_ref()[*idx]
    }
}
impl<'a> IndexMut<uint, f32> for &'a mut Quaternion {
    fn index_mut<'b>(&'b mut self, idx: &uint) -> &'b mut f32 {
        &mut self.get_mut_ref()[*idx]
    }
}
