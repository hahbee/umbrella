use std::default::Default;
use std::{collections, num, to_string, ops, iter, clone, fmt, hash, cmp};
use std::mem::{transmute, transmute_copy};
use std::raw::Repr;
use serialize;

pub struct Vec2([f32, ..2]);
pub struct Vec3([f32, ..3]);
pub struct Vec4([f32, ..4]);

impl fmt::Show for Vec2 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "<"));
        try!(self[0].fmt(f));
        try!(write!(f, ", "));
        try!(self[1].fmt(f));
        write!(f, ">")
    }
}
impl hash::Hash for Vec2 {
    fn hash(&self, state: &mut hash::sip::SipState) {
        let as_u32: u32 = unsafe { transmute(self[0]) };
        as_u32.hash(state);
        let as_u32: u32 = unsafe { transmute(self[1]) };
        as_u32.hash(state);
    }
}
impl cmp::PartialEq for Vec2 {
    fn eq(&self, rhs: &Vec2) -> bool {
        self[0] == rhs[0] && self[1] == rhs[1]
    }
}
impl clone::Clone for Vec2 {
    fn clone(&self) -> Vec2 {
        unsafe { transmute_copy(self) }
    }
}
impl fmt::Show for Vec3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "<"));
        try!(self[0].fmt(f));
        try!(write!(f, ", "));
        try!(self[1].fmt(f));
        try!(write!(f, ", "));
        try!(self[2].fmt(f));
        write!(f, ">")
    }
}
impl hash::Hash for Vec3 {
    fn hash(&self, state: &mut hash::sip::SipState) {
        let as_u32: u32 = unsafe { transmute(self[0]) };
        as_u32.hash(state);
        let as_u32: u32 = unsafe { transmute(self[1]) };
        as_u32.hash(state);
        let as_u32: u32 = unsafe { transmute(self[2]) };
        as_u32.hash(state);
    }
}
impl cmp::PartialEq for Vec3 {
    fn eq(&self, rhs: &Vec3) -> bool {
        self[0] == rhs[0] && self[1] == rhs[1] && self[2] == rhs[2]
    }
}
impl clone::Clone for Vec3 {
    fn clone(&self) -> Vec3 {
        unsafe { transmute_copy(self) }
    }
}
impl fmt::Show for Vec4 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "<"));
        try!(self[0].fmt(f));
        try!(write!(f, ", "));
        try!(self[1].fmt(f));
        try!(write!(f, ", "));
        try!(self[2].fmt(f));
        try!(write!(f, ", "));
        try!(self[3].fmt(f));
        write!(f, ">")
    }
}
impl hash::Hash for Vec4 {
    fn hash(&self, state: &mut hash::sip::SipState) {
        let as_u32: u32 = unsafe { transmute(self[0]) };
        as_u32.hash(state);
        let as_u32: u32 = unsafe { transmute(self[1]) };
        as_u32.hash(state);
        let as_u32: u32 = unsafe { transmute(self[2]) };
        as_u32.hash(state);
        let as_u32: u32 = unsafe { transmute(self[3]) };
        as_u32.hash(state);
    }
}
impl cmp::PartialEq for Vec4 {
    fn eq(&self, rhs: &Vec4) -> bool {
        self[0] == rhs[0] && self[1] == rhs[1] && self[2] == rhs[2] && self[3] == rhs[3]
    }
}
impl clone::Clone for Vec4 {
    fn clone(&self) -> Vec4 {
        unsafe { transmute_copy(self) }
    }
}

pub trait Vector<T>: Default + ops::Index<uint, f32> + ops::IndexMut<uint, f32> + collections::Collection + num::Zero + to_string::ToString + clone::Clone + iter::FromIterator<f32> + ops::Neg<Self> + ops::Add<Self, Self> + ops::Sub<Self, Self> + ops::Mul<Self, Self> + ops::Div<Self, Self> {
    fn identity() -> Self;
    fn smear(v: f32) -> Self;
    fn from_slice<'a>(p: &'a [f32]) -> Option<&'a Self>;

    fn get_ref<'a>(&'a self) -> &'a T;
    fn get_mut_ref<'a>(&'a mut self) -> &'a mut T;
    fn unwrap(self) -> T;
    fn wrap(v: T) -> Self;

    fn as_slice<'a>(&'a self) -> &'a [f32] {
        use std::slice::raw::buf_as_slice;
        unsafe {
            let ptr: *const f32 = transmute(self.get_ref());
            buf_as_slice(ptr,
                         self.len(),
                         |b| transmute(b) )
        }
    }

    #[inline]
    fn get_entry(&self, i: uint) -> f32 {
        (*self)[i]
    }
    fn set_entry(&mut self, i: uint, v: f32) {
        (*self)[i] = v;
    }
    #[inline]
    fn with(&self, i: uint, v: f32) -> Self {
        let mut s = self.clone();
        s.set_entry(i, v);
        s
    }

    #[inline]
    fn length_squared(&self) -> f32 {
        let sq = (*self) * (*self);
        iter::range(0u, self.len())
            .fold(0.0f32, |f, i| {
                f + sq[i]
            })
    }
    #[inline]
    fn length(&self) -> f32 {
        self.length_squared().sqrt()
    }
    #[inline]
    fn normalized(&self) -> (f32, Self) {
        let mut s = self.clone();
        let length = s.normalize();
        (length, s)
    }
    fn normalize(&mut self) -> f32 {
        let len_sq = self.length_squared();
        if len_sq == 0.0 { 0.0 }
        else {
            let len = len_sq.sqrt();
            let inv_len: Self = Vector::smear(len.recip());
            self.mul_mut(inv_len);
            len
        }
    }

    #[inline]
    fn mul_imm(&self, v: Self) -> Self {
        let mut s = self.clone();
        s.mul_mut(v);
        s
    }
    #[inline]
    fn div_imm(&self, v: Self) -> Self {
        let mut s = self.clone();
        s.div_mut(v);
        s
    }
    #[inline]
    fn add_imm(&self, v: Self) -> Self {
        let mut s = self.clone();
        s.add_mut(v);
        s
    }
    #[inline]
    fn sub_imm(&self, v: Self) -> Self {
        let mut s = self.clone();
        s.sub_mut(v);
        s
    }
    #[inline]
    fn scale_imm(&self, v: f32) -> Self {
        let smear: Self = Vector::smear(v);
        self.mul_imm(smear)
    }
    #[inline]
    fn scale_mut(&mut self, v: f32) {
        let smear: Self = Vector::smear(v);
        self.mul_mut(smear)
    }
    
    fn mul_mut(&mut self, v: Self) {
        for i in iter::range(0, self.len()) {
            (*self)[i] = (*self)[i] * v[i];
        }
    }
    fn div_mut(&mut self, v: Self) {
        for i in iter::range(0, self.len()) {
            (*self)[i] = (*self)[i] / v[i];
        }
    }
    fn add_mut(&mut self, v: Self) {
        for i in iter::range(0, self.len()) {
            (*self)[i] = (*self)[i] + v[i];
        }
    }
    fn sub_mut(&mut self, v: Self) {
        for i in iter::range(0, self.len()) {
            (*self)[i] = (*self)[i] - v[i];
        }
    }
}
pub trait CrossProduct: clone::Clone {
    #[inline]
    fn cross_product_imm(&self, v: Self) -> Self {
        let mut s = self.clone();
        s.cross_product_mut(v);
        s
    }
    fn cross_product_mut(&mut self, v: Self);
}
pub trait Vector2: clone::Clone + ops::Neg<Self> + ops::Index<uint, f32> {
    fn unit_x() -> Self;
    fn unit_y() -> Self;
    #[inline] fn neg_unit_x() -> Self {
        let ux: Self = Vector2::unit_x();
        ux.neg()
    }
    #[inline] fn neg_unit_y() -> Self {
        let uy: Self = Vector2::unit_y();
        uy.neg()
    }

    fn as_imm_vec2<'a>(&'a self) -> &'a Vec2 {
        unsafe { transmute(self) }
    }
    fn as_mut_vec2<'a>(&'a mut self) -> &'a mut Vec2 {
        unsafe { transmute(self) }
    }

    #[inline]
    fn mul_imm_vec2<T: Vector2>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.mul_mut_vec2(v);
        s
    }
    #[inline]
    fn div_imm_vec2<T: Vector2>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.div_mut_vec2(v);
        s
    }
    #[inline]
    fn add_imm_vec2<T: Vector2>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.add_mut_vec2(v);
        s
    }
    #[inline]
    fn sub_imm_vec2<T: Vector2>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.sub_mut_vec2(v);
        s
    }
    #[inline]
    fn scale_imm_vec2(&self, v: f32) -> Self {
        let mut s = self.clone();
        s.scale_mut_vec2(v);
        s
    }

    fn scale_mut_vec2(&mut self, v: f32);
    
    fn mul_mut_vec2<T: Vector2>(&mut self, v: T);
    fn div_mut_vec2<T: Vector2>(&mut self, v: T);
    fn add_mut_vec2<T: Vector2>(&mut self, v: T);
    fn sub_mut_vec2<T: Vector2>(&mut self, v: T);
}
pub trait Vector3: ops::Neg<Self> + Vector2 {
    fn unit_z() -> Self;
    #[inline] fn neg_unit_z() -> Self {
        let uz: Self = Vector3::unit_z();
        uz.neg()
    }
    fn as_imm_vec3<'a>(&'a self) -> &'a Vec3 {
        unsafe { transmute(self) }
    }
    fn as_mut_vec3<'a>(&'a mut self) -> &'a mut Vec3 {
        unsafe { transmute(self) }
    }

    #[inline]
    fn mul_imm_vec3<T: Vector3>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.mul_mut_vec3(v);
        s
    }
    #[inline]
    fn div_imm_vec3<T: Vector3>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.div_mut_vec3(v);
        s
    }
    #[inline]
    fn add_imm_vec3<T: Vector3>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.add_mut_vec3(v);
        s
    }
    #[inline]
    fn sub_imm_vec3<T: Vector3>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.sub_mut_vec3(v);
        s
    }
    #[inline]
    fn scale_imm_vec3(&self, v: f32) -> Self {
        let mut s = self.clone();
        s.scale_mut_vec3(v);
        s
    }
    fn scale_mut_vec3(&mut self, v: f32);
    
    fn mul_mut_vec3<T: Vector3>(&mut self, v: T);
    fn div_mut_vec3<T: Vector3>(&mut self, v: T);
    fn add_mut_vec3<T: Vector3>(&mut self, v: T);
    fn sub_mut_vec3<T: Vector3>(&mut self, v: T);
}
pub trait Vector4: clone::Clone + ops::Neg<Self> + Vector2 + Vector3 {
    fn unit_w() -> Self;
    #[inline] fn neg_unit_w() -> Self {
        let uw: Self = Vector4::unit_w();
        uw.neg()
    }
    fn as_imm_vec4<'a>(&'a self) -> &'a Vec4 {
        unsafe { transmute(self) }
    }
    fn as_mut_vec4<'a>(&'a mut self) -> &'a mut Vec4 {
        unsafe { transmute(self) }
    }

    #[inline]
    fn mul_imm_vec4<T: Vector4>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.mul_mut_vec4(v);
        s
    }
    #[inline]
    fn div_imm_vec4<T: Vector4>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.div_mut_vec4(v);
        s
    }
    #[inline]
    fn add_imm_vec4<T: Vector4>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.add_mut_vec4(v);
        s
    }
    #[inline]
    fn sub_imm_vec4<T: Vector4>(&self, v: T) -> Self {
        let mut s = self.clone();
        s.sub_mut_vec4(v);
        s
    }
    #[inline]
    fn scale_imm_vec4(&self, v: f32) -> Self {
        let mut s = self.clone();
        s.scale_mut_vec4(v);
        s
    }
    fn scale_mut_vec4(&mut self, v: f32);
    
    fn mul_mut_vec4<T: Vector4>(&mut self, v: T);
    fn div_mut_vec4<T: Vector4>(&mut self, v: T);
    fn add_mut_vec4<T: Vector4>(&mut self, v: T);
    fn sub_mut_vec4<T: Vector4>(&mut self, v: T);
}
impl Vector2 for Vec2 {
    fn unit_x() -> Vec2 {
        Vector::wrap([1.0, 0.0])
    }
    fn unit_y() -> Vec2 {
        Vector::wrap([0.0, 1.0])
    }
    fn scale_mut_vec2(&mut self, v: f32) {
        let smear: Vec2 = Vector::smear(v);
        self.mul_mut_vec2(smear)
    }
    fn mul_mut_vec2<T: Vector2>(&mut self, v: T) {
        *self = (*self) * (*v.as_imm_vec2());
    }
    fn div_mut_vec2<T: Vector2>(&mut self, v: T) {
        *self = (*self) / (*v.as_imm_vec2());
    }
    fn add_mut_vec2<T: Vector2>(&mut self, v: T) {
        *self = (*self) + (*v.as_imm_vec2());
    }
    fn sub_mut_vec2<T: Vector2>(&mut self, v: T) {
        *self = (*self) - (*v.as_imm_vec2());
    }
}
impl Vector2 for Vec3 {
    fn unit_x() -> Vec3 {
        Vector::wrap([1.0, 0.0, 0.0])
    }
    fn unit_y() -> Vec3 {
        Vector::wrap([0.0, 1.0, 0.0])
    }
    fn scale_mut_vec2(&mut self, v: f32) {
        let smear: Vec2 = Vector::smear(v);
        self.mul_mut_vec2(smear)
    }
    fn mul_mut_vec2<T: Vector2>(&mut self, v: T) {
        let r = (*self.as_imm_vec2()) * (*v.as_imm_vec2());
        *self = Vector::wrap([r[0], r[1], (*self)[2]]);
    }
    fn div_mut_vec2<T: Vector2>(&mut self, v: T) {
        let r = (*self.as_imm_vec2()) / (*v.as_imm_vec2());
        *self = Vector::wrap([r[0], r[1], (*self)[2]]);
    }
    fn add_mut_vec2<T: Vector2>(&mut self, v: T) {
        let r = (*self.as_imm_vec2()) + (*v.as_imm_vec2());
        *self = Vector::wrap([r[0], r[1], (*self)[2]]);
    }
    fn sub_mut_vec2<T: Vector2>(&mut self, v: T) {
        let r = (*self.as_imm_vec2()) - (*v.as_imm_vec2());
        *self = Vector::wrap([r[0], r[1], (*self)[2]]);
    }
}
impl Vector3 for Vec3 {
    fn unit_z() -> Vec3 {
        Vector::wrap([0.0, 0.0, 1.0])
    }
    fn scale_mut_vec3(&mut self, v: f32) {
        let smear: Vec3 = Vector::smear(v);
        self.mul_mut_vec3(smear)
    }
    fn mul_mut_vec3<T: Vector3>(&mut self, v: T) {
        *self = (*self) * (*v.as_imm_vec3());
    }
    fn div_mut_vec3<T: Vector3>(&mut self, v: T) {
        *self = (*self) / (*v.as_imm_vec3());
    }
    fn add_mut_vec3<T: Vector3>(&mut self, v: T) {
        *self = (*self) + (*v.as_imm_vec3());
    }
    fn sub_mut_vec3<T: Vector3>(&mut self, v: T) {
        *self = (*self) - (*v.as_imm_vec3());
    }
}
impl Vector2 for Vec4 {
    fn unit_x() -> Vec4 {
        Vector::wrap([1.0, 0.0, 0.0, 0.0])
    }
    fn unit_y() -> Vec4 {
        Vector::wrap([0.0, 1.0, 0.0, 0.0])
    }
    fn scale_mut_vec2(&mut self, v: f32) {
        let smear: Vec2 = Vector::smear(v);
        self.mul_mut_vec2(smear)
    }
    fn mul_mut_vec2<T: Vector2>(&mut self, v: T) {
        let r = (*self.as_imm_vec2()) * (*v.as_imm_vec2());
        *self = Vector::wrap([r[0], r[1], (*self)[2], (*self)[3]]);
    }
    fn div_mut_vec2<T: Vector2>(&mut self, v: T) {
        let r = (*self.as_imm_vec2()) / (*v.as_imm_vec2());
        *self = Vector::wrap([r[0], r[1], (*self)[2], (*self)[3]]);
    }
    fn add_mut_vec2<T: Vector2>(&mut self, v: T) {
        let r = (*self.as_imm_vec2()) + (*v.as_imm_vec2());
        *self = Vector::wrap([r[0], r[1], (*self)[2], (*self)[3]]);
    }
    fn sub_mut_vec2<T: Vector2>(&mut self, v: T) {
        let r = (*self.as_imm_vec2()) - (*v.as_imm_vec2());
        *self = Vector::wrap([r[0], r[1], (*self)[2], (*self)[3]]);
    }
}
impl Vector3 for Vec4 {
    fn unit_z() -> Vec4 {
        Vector::wrap([0.0, 0.0, 1.0, 0.0])
    }
    fn scale_mut_vec3(&mut self, v: f32) {
        let smear: Vec3 = Vector::smear(v);
        self.mul_mut_vec3(smear)
    }
    fn mul_mut_vec3<T: Vector3>(&mut self, v: T) {
        let r = (*self.as_imm_vec3()) * (*v.as_imm_vec3());
        *self = Vector::wrap([r[0], r[1], r[2], (*self)[3]]);
    }
    fn div_mut_vec3<T: Vector3>(&mut self, v: T) {
        let r = (*self.as_imm_vec3()) / (*v.as_imm_vec3());
        *self = Vector::wrap([r[0], r[1], r[2], (*self)[3]]);
    }
    fn add_mut_vec3<T: Vector3>(&mut self, v: T) {
        let r = (*self.as_imm_vec3()) + (*v.as_imm_vec3());
        *self = Vector::wrap([r[0], r[1], r[2], (*self)[3]]);
    }
    fn sub_mut_vec3<T: Vector3>(&mut self, v: T) {
        let r = (*self.as_imm_vec3()) - (*v.as_imm_vec3());
        *self = Vector::wrap([r[0], r[1], r[2], (*self)[3]]);
    }
}
impl Vector4 for Vec4 {
    fn unit_w() -> Vec4 {
        Vector::wrap([0.0, 0.0, 0.0, 1.0])
    }

    fn scale_mut_vec4(&mut self, v: f32) {
        let smear: Vec4 = Vector::smear(v);
        self.mul_mut_vec4(smear)
    }

    fn mul_mut_vec4<T: Vector4>(&mut self, v: T) {
        *self = (*self) * (*v.as_imm_vec4());
    }
    fn div_mut_vec4<T: Vector4>(&mut self, v: T) {
        *self = (*self) / (*v.as_imm_vec4());
    }
    fn add_mut_vec4<T: Vector4>(&mut self, v: T) {
        *self = (*self) + (*v.as_imm_vec4());
    }
    fn sub_mut_vec4<T: Vector4>(&mut self, v: T) {
        *self = (*self) - (*v.as_imm_vec4());
    }
}
impl Vector<[f32, ..2]> for Vec2 {
    fn identity() -> Vec2 {
        Vector::wrap([1.0, 1.0])
    }
    fn smear(v: f32) -> Vec2 {
        Vector::wrap([v, v])
    }
    fn from_slice<'a>(p: &'a [f32]) -> Option<&'a Vec2> {
        let slice_repr = p.repr();
        if slice_repr.len < 2 { None }
        else { Some(unsafe { transmute(slice_repr.data) }) }
    }

    fn get_ref<'a>(&'a self) -> &'a [f32, ..2] {
        let &Vec2(ref inner) = self;
        inner
    }
    fn get_mut_ref<'a>(&'a mut self) -> &'a mut [f32, ..2] {
        let &Vec2(ref mut inner) = self;
        inner
    }
    fn unwrap(self) -> [f32, ..2] {
        let Vec2(inner) = self;
        inner
    }
    fn wrap(v: [f32, ..2]) -> Vec2 {
        Vec2(v)
    }

    fn normalize(&mut self) -> f32 {
        let len_sq = self.length_squared();
        if len_sq == 0.0 { 0.0 }
        else {
            let len = len_sq.sqrt();
            let inv_len: Vec2 = Vector::smear(len.recip());
            self.mul_mut(inv_len);
            len
        }
    }
}
impl Vector<[f32, ..3]> for Vec3 {
    fn identity() -> Vec3 {
        Vector::wrap([1.0, 1.0, 1.0])
    }
    fn smear(v: f32) -> Vec3 {
        Vector::wrap([v, v, v])
    }
    fn from_slice<'a>(p: &'a [f32]) -> Option<&'a Vec3> {
        let slice_repr = p.repr();
        if slice_repr.len < 3 { None }
        else { Some(unsafe { transmute(slice_repr.data) }) }
    }

    fn get_ref<'a>(&'a self) -> &'a [f32, ..3] {
        let &Vec3(ref inner) = self;
        inner
    }
    fn get_mut_ref<'a>(&'a mut self) -> &'a mut [f32, ..3] {
        let &Vec3(ref mut inner) = self;
        inner
    }
    fn unwrap(self) -> [f32, ..3] {
        let Vec3(inner) = self;
        inner
    }
    fn wrap(v: [f32, ..3]) -> Vec3 {
        Vec3(v)
    }
}
impl CrossProduct for Vec3 {
    fn cross_product_mut(&mut self, v: Vec3) {
        let ss = self.unwrap();
        let vs = v.unwrap();
        (*self)[0] = ss[1] * vs[2] - ss[2] * vs[1];
        (*self)[1] = ss[2] * vs[0] - ss[0] * vs[2];
        (*self)[2] = ss[0] * vs[1] - ss[1] * vs[0];
    }
}
impl Vector<[f32, ..4]> for Vec4 {
    fn identity() -> Vec4 {
        Vector::smear(1.0f32)
    }
    fn smear(v: f32) -> Vec4 {
        Vector::wrap([v, ..4])
    }
    fn from_slice<'a>(p: &'a [f32]) -> Option<&'a Vec4> {
        let slice_repr = p.repr();
        if slice_repr.len < 4 { None }
        else { Some(unsafe { transmute(slice_repr.data) }) }
    }

    fn get_ref<'a>(&'a self) -> &'a [f32, ..4] {
        let &Vec4(ref inner) = self;
        inner
    }
    fn get_mut_ref<'a>(&'a mut self) -> &'a mut [f32, ..4] {
        let &Vec4(ref mut inner) = self;
        inner
    }
    fn unwrap(self) -> [f32, ..4] {
        let Vec4(inner) = self;
        inner
    }
    fn wrap(v: [f32, ..4]) -> Vec4 {
        Vec4(v)
    }

    fn normalize(&mut self) -> f32 {
        let len_sq = self.length_squared();
        if len_sq == 0.0 { 0.0 }
        else {
            let len = len_sq.sqrt();
            let inv_len = len.recip();
            let inv_len: Vec4 = Vector::smear(inv_len);
            self.mul_mut(inv_len);
            len
        }
    }
}

impl super::Position for Vec3 {
    #[inline]
    fn get_position(&self) -> Vec3 {
        self.clone()
    }
    #[inline]
    fn set_position<T: super::Position>(&mut self, v: &T) {
        *self = v.get_position();
    }
}
impl super::Position for Vec4 {
    #[inline]
    fn get_position(&self) -> Vec3 {
        self.as_imm_vec3().clone()
    }
    #[inline]
    fn set_position<T: super::Position>(&mut self, v: &T) {
        let v = v.get_position();
        *self = Vector::wrap([v[0], v[1], v[2], (*self)[3]]);
    }
}

macro_rules! _impl_traits(
    ($ty:ty = [$len:expr]) => {
        impl Default for $ty {
            #[inline]
            fn default() -> $ty {
                Vector::identity()
            }
        }
        impl ops::Index<uint, f32> for $ty {
            fn index<'b>(&'b self, idx: &uint) -> &'b f32 {
                &self.get_ref()[*idx]
            }
        }
        impl ops::IndexMut<uint, f32> for $ty {
            fn index_mut<'b>(&'b mut self, idx: &uint) -> &'b mut f32 {
                &mut self.get_mut_ref()[*idx]
            }
        }
        impl<'a> ops::Index<uint, f32> for &'a $ty {
            fn index<'b>(&'b self, idx: &uint) -> &'b f32 {
                &self.get_ref()[*idx]
            }
        }
        impl<'a> ops::IndexMut<uint, f32> for &'a mut $ty {
            fn index_mut<'b>(&'b mut self, idx: &uint) -> &'b mut f32 {
                &mut self.get_mut_ref()[*idx]
            }
        }
        impl num::Zero for $ty {
            fn zero() -> $ty {
                Vector::smear(0.0)
            }
            
            fn is_zero(&self) -> bool {
                iter::range(0u, self.len()).all(|i| self[i] == 0.0 )
            }
        }
        impl iter::FromIterator<f32> for $ty {
            fn from_iter<T: Iterator<f32>>(mut iterator: T) -> $ty {
                let mut f: $ty = Default::default();
                for i in iter::range(0u, $len) {
                    match iterator.next() {
                        Some(v) => f = f.with(i, v),
                        None => break,
                    }
                }
                f
            }
        }
        impl ops::Add<$ty, $ty> for $ty {
            #[inline(always)]
            fn add(&self, rhs: &$ty) -> $ty {
                self.add_imm(*rhs)
            }
        }
        impl ops::Mul<$ty, $ty> for $ty {
            #[inline(always)]
            fn mul(&self, rhs: &$ty) -> $ty {
                self.mul_imm(*rhs)
            }
        }
        impl ops::Div<$ty, $ty> for $ty {
            #[inline(always)]
            fn div(&self, rhs: &$ty) -> $ty {
                self.div_imm(*rhs)
            }
        }
        impl ops::Sub<$ty, $ty> for $ty {
            #[inline(always)]
            fn sub(&self, rhs: &$ty) -> $ty {
                self.sub_imm(*rhs)
            }
        }
        impl collections::Collection for $ty {
            #[inline(always)]
            fn len(&self) -> uint { $len }
        }
        impl ops::Neg<$ty> for $ty {
            fn neg(&self) -> $ty {
                self.scale_imm(-1.0)
            }
        }
        impl $ty {
            pub fn from_array(v: [f32, ..$len]) -> $ty {
                let mut f: $ty = Default::default();
                for i in iter::range(0u, $len) {
                    f = f.with(i, v[i]);
                }
                f
            }
        }
        impl<E, D: serialize::Decoder<E>> serialize::Decodable<D, E> for $ty {
            fn decode(d: &mut D) -> Result<$ty, E> {
                let mut dest: $ty = Default::default();
                for i in iter::range(0u, $len) {
                    dest = dest.with(i, try!(d.read_f32()));
                }
                Ok(dest)
            }
        }
        impl<E, S: serialize::Encoder<E>> serialize::Encodable<S, E> for $ty {
            fn encode(&self, e: &mut S) -> Result<(), E> {
                for i in iter::range(0u, $len) {
                    try!(e.emit_f32(self[i]));
                }
                Ok(())
            }
        }
    };
)
_impl_traits!(Vec2 = [2])
_impl_traits!(Vec3 = [3])
_impl_traits!(Vec4 = [4])

