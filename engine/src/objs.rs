
#[deriving(Hash, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct HiveId(u64);
pub struct Hive {
    id: HiveId,
    pub name: String,
}
#[deriving(Hash, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct SwarmId(u64);
pub struct Swarm {
    id: SwarmId,
    pub name: String,
    
}
#[deriving(Hash, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct BeeId(u64);
pub struct Bee {
    id: BeeId,
    pub name: String,
    
}
