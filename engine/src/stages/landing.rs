
use ppapi::font;
use ppapi::font::{Description};
use infra;
use infra::{StageAnimationData, StageAnimationResult, Frame};
use ui;
use ui::Element;
use animation;
use animation::Animatible;
use math::vector::{Vector, Vec3};
use gl;

pub struct Landing {
    features: Element,
    login: Element,
}
static LOGIN_TITLE_KEY: infra::Key = infra::Key(0);
static LOGIN_LOGIN_KEY: infra::Key = infra::Key(1);
static LOGIN_PASSWORD_KEY: infra::Key = infra::Key(2);
static LOGIN_REMEMBER_KEY: infra::Key = infra::Key(3);
static LOGIN_SUBMIT_KEY: infra::Key = infra::Key(4);
static LOGIN_CLEAR_KEY: infra::Key = infra::Key(5);
static LOGIN_REGISTER_KEY: infra::Key = infra::Key(6);
static LOGIN_FORGOT_KEY: infra::Key = infra::Key(7);
static LOGIN_CONTAINER_KEY: infra::Key = infra::Key(8);

impl Landing {
    pub fn new(context: &mut infra::ContextData) -> Landing {
        Landing {
            features: Landing::new_features(context),
            login: Landing::new_login(context),
        }
    }
    fn new_login(context: &mut infra::ContextData) -> Element {
        use gl::mesh::prefabs::PLANE_MESH;

        let mut shdr_mgr_borrow = context.shader.borrow_mut();
        let shdr_mgr = &mut *shdr_mgr_borrow;
        let elem_shdr = |x: f32, y: f32, z: f32, w: f32| {
            ui::build_shader_program(&context.context,
                                     shdr_mgr,
                                     Vector::wrap([x, y, z, w]))
        };
        let mut login = Element::new(PLANE_MESH, elem_shdr(1.0f32, 1.0f32, -1.0f32, -1.0f32));
        login.pos = Animatible::new(Vec3::from_array([0.75, 0.5, 0.0]));

        login.put_child(LOGIN_TITLE_KEY, {
            let mut e = Element::new(PLANE_MESH, elem_shdr(0.1f32, 0.1f32, -0.1f32, -0.1f32));
            let font = context.instance.create_font
                (&Description::new_from_family(font::SerifFamily));
            let text = ui::Text::new("Login",
                                     font.unwrap(),
                                     0,
                                     None,
                                     false,
                                     false);
            e.set_content(ui::TextContent(text));
            e
        });
        login.put_child(LOGIN_LOGIN_KEY, {
            let mut e = Element::new(PLANE_MESH, elem_shdr(1.0f32, 1.0f32, -1.0f32, -1.0f32));
            let font = context.instance.create_font
                (&Description::new_from_family(font::SerifFamily));
            let text = ui::Text::new("Username",
                                     font.unwrap(),
                                     0,
                                     None,
                                     false,
                                     true);
            e.set_content(ui::TextContent(text));
            e
        });
        login.put_child(LOGIN_PASSWORD_KEY, {
            let mut e = Element::new(PLANE_MESH, elem_shdr(1.0f32, 1.0f32, -1.0f32, -1.0f32));
            let font = context.instance.create_font
                (&Description::new_from_family(font::SerifFamily));
            let text = ui::Text::new("",
                                     font.unwrap(),
                                     0,
                                     Some('*'),
                                     false,
                                     true);
            e.set_content(ui::TextContent(text));
            e
        });
        login
    }
    fn new_features(ctxt: &mut infra::ContextData) -> Element {
        use gl::mesh::prefabs::PLANE_MESH;
        let mut shdr_mgr_borrow = ctxt.shader.borrow_mut();
        let shdr_mgr = &mut *shdr_mgr_borrow;
        let elem_shdr = |x: f32, y: f32, z: f32, w: f32| {
            ui::build_shader_program(&ctxt.context,
                                     shdr_mgr,
                                     Vector::wrap([x, y, z, w]))
        };
        Element::new(PLANE_MESH, elem_shdr(1.0f32, 1.0f32, -1.0f32, -1.0f32))
    }
}

impl animation::Animated<StageAnimationData, (StageAnimationData, StageAnimationResult)> for Landing {
    fn animate(&mut self,
               frame: &Frame,
               mut ctxt: StageAnimationData) -> (StageAnimationData, StageAnimationResult) {
        
        (ctxt, infra::Continue)
    }
}

impl infra::Stage for Landing {
    fn render(&self, ctxt: &infra::ContextData, cam: &gl::Camera) {
        use ppapi::gles::consts;
        ctxt.context.clear(consts::COLOR_BUFFER_BIT |
                           consts::DEPTH_BUFFER_BIT |
                           consts::STENCIL_BUFFER_BIT);
    }
}
