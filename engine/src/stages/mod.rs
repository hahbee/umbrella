use ppapi;

use infra;
use infra::{StageAnimationData, StageAnimationResult, Frame, RenditionData};

use animation;

pub mod landing;

pub enum Stage {
    // The "stage" used before we've received the instance's view:
    UninitializedStage(fn(&mut RenditionData, ppapi::View) -> Stage),
    LandingStage(landing::Landing),
}
impl Stage {
    pub fn is_uninitialized(&self) -> bool {
        match self {
            &UninitializedStage(_) => true,
            _ => false,
        }
    }
    pub fn initialize(&mut self, ctxt: &mut RenditionData, view: ppapi::View) {
        let f = match self {
            &UninitializedStage(f) => f,
            _ => unreachable!(),
        };
        *self = f(ctxt, view);
    }
    
    pub fn render(&self, ctxt: &mut RenditionData) {
        use ppapi::gles::consts;
        ctxt.context.clear(consts::COLOR_BUFFER_BIT |
                           consts::DEPTH_BUFFER_BIT |
                           consts::STENCIL_BUFFER_BIT);
    }
    pub fn view_changed(&mut self, view: ppapi::View) {}
}

impl animation::Animated<StageAnimationData, (StageAnimationData, StageAnimationResult)> for Stage {
    fn animate(&mut self,
               frame: &Frame,
               mut ctxt: StageAnimationData) -> (StageAnimationData, StageAnimationResult) {
        
        (ctxt, infra::Continue)
    }
}
