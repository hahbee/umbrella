
use math::vector::{Vec3, Vector};

use std::vec::Vec;
use std::rand::{StdRng, Rng};
use std::iter::range;

static FIELD_RADIUS: uint = 1000;
static POSITION_COUNT: uint = 200;

pub struct StarField {
    positions: Vec<Vec3>,
    sizes:     Vec<f32>,
}

impl StarField {
    pub fn new() -> StarField {
        let mut positions = Vec::with_capacity(POSITION_COUNT);
        
        let mut rand = StdRng::new().unwrap();
        let two: Vec3 = Vector::smear(2.0f32);
        for _ in range(0, POSITION_COUNT) {
            let pos: [f32, ..3] = [rand.gen(),
                                   rand.gen(),
                                   rand.gen()];
            let pos: Vec3 = Vec3::from_array(pos);
            let pos = pos.mul(&two).sub(&Vector::smear(1.0f32));
            let len = pos.length();
            let radius = rand.gen::<f32>() * FIELD_RADIUS as f32;
            let vec = Vector::smear((1.0 - radius.powf(3.0)) / (len as f32));
            let pos = pos.mul(&vec);
            positions.push(pos);
        }

        let sizes = Vec::from_elem(POSITION_COUNT, 3.0f32);
        StarField {
            positions: positions,
            sizes: sizes,
        }
    }
}
