use ppapi::{Ticks};
use ppapi::input::{KeyboardClass, KeyboardClassEvent,
                   MouseClass,    MouseClassEvent,
                   WheelClass,    WheelClassEvent,
                   TouchClass,    TouchClassEvent,
                   IMEClass,      IMEClassEvent,
                   Event, Class};

use infra::Key;
use std::collections::ringbuf::RingBuf;
use std::collections::trie::TrieMap;
use std::collections::hashmap::HashMap;
use std::collections::Deque;
use std::default;
use std::cell::RefCell;
use std::rc::{Rc, Weak};
use super::Element;

pub trait Handler {
    fn handle_untyped(self, event: Class, elem: &mut Element) -> Box<Handler>;
}

#[deriving(Eq, Clone, Hash, PartialEq, Ord, PartialOrd)]
pub struct Handle(uint);

impl Handle {
    fn next_handle(&mut self) -> Handle {
        let handle = self.clone();
        *self = Handle(self.deref() + 1);
        handle
    }
    fn deref(&self) -> uint {
        let &Handle(inner) = self;
        inner
    }
}
impl default::Default for Handle {
    fn default() -> Handle {
        Handle(0)
    }
}

struct Handlers {
    handlers: TrieMap<Box<Handler>>,
    children: Box<TrieMap<Handlers>>,
}
impl Handlers {
    fn with_imm_child<Take, Ret>(&self,
                                 mut key: Vec<Key>,
                                 take: Take,
                                 f: |&Handlers, Take| -> Ret) -> Option<Ret> {
        assert!(key.len() > 0);
        let this = key.shift().unwrap();
        match self.children.find(&(*this as uint)) {
            Some(ref child) if key.len() == 0 => Some(f(*child, take)),
            Some(ref child) => child.with_imm_child(key, take, f),
            None => None,
        }
    }
    fn with_mut_child<Take, Ret>(&mut self,
                                 mut key: Vec<Key>,
                                 take: Take,
                                 f: |&mut Handlers, Take| -> Ret) -> Option<Ret> {
        assert!(key.len() > 0);
        let this = key.shift().unwrap();
        match self.children.find_mut(&(*this as uint)) {
            Some(ref mut child) if key.len() == 0 => Some(f(*child, take)),
            Some(ref mut child) => child.with_mut_child(key, take, f),
            None => None,
        }
    }
}
impl default::Default for Handlers {
    fn default() -> Handlers {
        Handlers {
            handlers: TrieMap::new(),
            children: box TrieMap::new(),
        }
    }
}

pub struct Manager__ {
    handle: Handle,

    by_handle: HashMap<Handle, Vec<Key>>,
    by_key: Handlers,

    kb_focus: Option<Vec<Key>>,

    mouse_hover_ticks: Ticks,
    mouse_hover_handle: Option<Handle>,


    events: RingBuf<Class>,
}
pub type Manager_ = RefCell<Manager__>;
pub type StrongManager = Rc<Manager_>;
pub type WeakManager   = Weak<Manager_>;

impl Manager__ {
    pub fn on_input_event(&mut self, event: Class) {
        self.events.push(event)
    }

    pub fn add_handler(&mut self,
                       key: Vec<Key>,
                       handler: Box<Handler>) -> Handle {
        let handle = self.handle.next_handle();
        self.by_key.with_mut_child(key,
                                   handler,
                                   |handlers, handler| {
                                       handlers.handlers.insert(handle.deref(), handler)
                                   });
        handle
    }
    pub fn handle_events<'a>(&mut self, elems: &[(Key, &'a mut Element)]) {
        self.events.pop_front().while_some(|event| {
            match event {
                KeyboardClass(_) => {
                    match self.kb_focus {
                        Some(ref key_chain) => {
                            
                        }
                        None => {}
                    }
                }
                MouseClass(_) => {
                    for &(ref key, ref elem) in elems.iter() {
                        
                    }
                }
                _ => error!("unimplemented"),
            }
            self.events.pop_front()
        });
    }
}

pub fn new_mgr() -> StrongManager {
    use std::default::Default;
    Rc::new(RefCell::new(Manager__ {
        mouse_hover_ticks: Default::default(),
        mouse_hover_handle: Default::default(),

        handle: Default::default(),
        by_handle: HashMap::new(),
        by_key: Default::default(),
        kb_focus: None,
        events: RingBuf::with_capacity(5),
    }))
}
