use math;
use infra;
use infra::{Key, DirtyCount, ContextData};
use animation::{Animatible, Animated};
use std::option::{Option, Some, None};
use std::collections::hashmap::HashMap;
use std::cell::{Cell, RefCell};
use std::default::Default;
use std::ops::Drop;
use std::cmp;
use std::num::Zero;
use libc;

use ppapi;
use ppapi::{Point, StringVar, ImageData, Font, Size, Instance, Context3d};
use ppapi::font::{Metrics};
use ppapi::gles::{TextureBuffer};
use ppapi::gles::traits::GenBuffer;
use ImageMap = ppapi::imagedata::Map;

use gl;
use gl::{SingleFloatScalar, Vec4Type, FVec4Value};
use gl::shader;
use gl::shader::{UniformRole, CustomUniformRole};
use gl::material::{Material, TextureUnit};

use math::vector::Vec4;

pub mod input;

pub struct Scene;

pub type ClipRegion = math::Vec2;
pub type AbsRect = ppapi::Rect;

pub trait ShiftRect {
    fn shift_point(&self, pos: Point) -> ppapi::Rect;
}
impl ShiftRect for ppapi::ffi::Struct_PP_Rect {
    fn shift_point(&self, pos: Point) -> ppapi::Rect {
        ppapi::ffi::Struct_PP_Rect {
            point: self.point + pos,
            size:  self.size,
        }
    }
}
#[deriving(Clone)]
pub enum AxisRegion<T> {
    /// size based on children
    /// use None for both min && max for unbounded.
    BoundedRegion(Option<T>,  // min size
                  Option<T>), // max size

    /// relative to parent
    RelRegion(T),

    /// static (doesn't change with parent). screen space relative.
    /// this one really only make sense for top level elements.
    StaticRegion(T),
}
impl<T> Default for AxisRegion<T> {
    fn default() -> AxisRegion<T> {
        BoundedRegion(None, None)
    }
}
impl<T> AxisRegion<T> {
}
#[deriving(Clone)]
pub struct Region {
    width:  AxisRegion<f32>,
    height: AxisRegion<f32>,
}
impl Default for Region {
    fn default() -> Region {
        Region {
            width: Default::default(),
            height: Default::default(),
        }
    }
}
#[deriving(Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
enum ExactRegion<T> {
    ExactStaticRegion(T),
    ExactRelRegion(T),
}
impl<T: Default + Ord + Clone> ExactRegion<T> {
    pub fn bind<TIter: Iterator<ExactRegion<T>>>(min: Option<T>,
                                                 max: Option<T>,
                                                 iter: TIter) -> ExactRegion<T> {
        let mut iter = iter
            .filter(|w| match w {
                &ExactRelRegion(_) => true,
                _ => false,
            } );
        let mut es_max = None;
        for region in iter {
            let region = match region {
                ExactRelRegion(region) => region,
                _ => unreachable!(),
            };
            es_max = Some
                (es_max.map_or(region.clone(), |prev_region: T| {
                    if prev_region.gt(&region) {
                        prev_region
                    } else {
                        region.clone()
                    }
                }) );
        }
        let es_max = es_max.or(min).or(max.clone()).unwrap_or(Default::default());
        ExactRelRegion(if max.clone().map_or(false, |max| max <= es_max ) {
            max.unwrap()
        } else {
            es_max
        })
    }
}

pub fn bound(min: Option<f32>, max: Option<f32>, value: f32) -> f32 {
    min
        .and_then(|min| {
            if min > value {
                Some(min)
            } else {
                None
            }
        })
        .or_else(|| max.and_then(|max| {
            if max < value {
                Some(max)
            } else {
                None
            }
        }))
        .unwrap_or(value)
}
pub struct Text {
    pub font:     ppapi::Font,
    pub color:    u32,
    pub mask:     Option<char>,        // a mask (think: passwords)
    pub rtl:      bool,
    pub text:     StringVar,
    pub editable: bool,

    img:          infra::InternalCache<(StringVar, Font), ppapi::imagedata::Map>,
    measurements: infra::InternalCache<(StringVar, Font), (i32, Metrics)>,
    mat:          infra::InternalCache<(StringVar, Font), Material>,
}
impl infra::CacheChecker<(StringVar, Font)> for Text {
    fn is_fresh(&self, &(ref str, ref font): &(StringVar, Font)) -> bool {
        self.font == *font && self.text == *str
    }
    fn fresh_key(&self) -> (StringVar, Font) {
        (self.text.clone(), self.font.clone())
    }
}
impl Text {
    pub fn new<TStr: ppapi::ToStringVar>(text:  TStr,
                                         font:  ppapi::Font,
                                         color: u32,
                                         mask:  Option<char>,
                                         rtl:   bool,
                                         editable: bool) -> Text {
        use infra::InternalCache;
        let text = text.to_string_var();
        Text {
            font: font.clone(),
            color: color,
            mask: mask,
            rtl: rtl,
            text: text.clone(),
            editable: editable,
            
            img:          InternalCache::new_stale(),
            measurements: InternalCache::new_stale(),
            mat:          InternalCache::new_stale(),
        }
    }
    pub fn measure(&self) -> Option<(i32, Metrics)> {
        self.measurements.get_opt(self, |_| {
            let width = self.font.measure_text(&self.text, self.rtl, false);
            width.and_then(|width| {
                self.font.describe().map(|(_, m)| (width, m) )
            })
        })
    }
    /// width, in pixels.
    pub fn width(&self) -> Option<i32> {
        self.measure().map(|(w, _)| w )
    }
    /// height, in pixels.
    pub fn height(&self) -> Option<i32> {
        self.measure().map(|(_, m)| m.height )
    }
    pub fn size(&self) -> Option<Size> {
        self.width().and_then(|width| {
            self.height().map(|height| {
                Size {
                    width:  width as u32,
                    height: height as u32,
                }
            })
        })
    }
    pub fn render(&self, ctxt: &mut Context3d) -> Option<Material> {
        use ppapi::imagedata::RGBA;
        fn raster(this: &Text,
                  map:  ImageMap) -> Option<ImageMap> {
            let code = this.font.draw_text(&map.img,
                                           &this.text,
                                           this.rtl,
                                           false,
                                           Zero::zero(),
                                           this.color as libc::uint32_t,
                                           None,
                                           true);
            match code {
                ppapi::Ok => Some(map),
                _ => {
                    error!("text rasterization returned `{}` while rasterizing: \
                           `{}`", code, this.text);
                    None
                },
            }
        }
        fn upload(ctxt: &mut Context3d,
                  mat: Option<Material>,
                  map: &ImageMap) -> Option<Material> {
            use ppapi::gles::{RgbaTexFormat};
            use ppapi::gles::consts::{TEXTURE_2D, UNSIGNED_BYTE};
            use ppapi::imagedata::MapImpl;
            // upload the rasterized text to the GPU
            let tex: TextureBuffer = GenBuffer::gen_single(ctxt);
            let bound = tex.bind(ctxt, TEXTURE_2D);
            map.with_imm_vec(|buf, desc| {
                // FIXME
                assert!(desc.line_stride == desc.size.width * 4);
                bound.image_2d(ctxt,
                               0,
                               RgbaTexFormat,
                               RgbaTexFormat,
                               desc.size,
                               UNSIGNED_BYTE,
                               Some(buf));
            });
            mat
        }
        self.mat.get_opt(self, |mut prev| {
            self.img
                .get_opt(self, |prev| {
                    drop(prev);
                    self.size().and_then(|size| {
                        Instance::current()
                            // note: force the image format to RGBA because that's a format
                            // OpenGL ES 2 supports.
                            .create_image(Some(RGBA), size, true)
                            .map(|img| img.map() )
                            .and_then(|map| raster(self, map) )
                    })
                })
                .and_then(|map| upload(ctxt, prev.take(), &map) )
        })
    }
}

impl Animated<(), bool> for Text {
    fn animate(&mut self, _frame: &infra::Frame, _: ()) -> bool {
        self.mat.is_fresh(self)
    }
}

pub struct Image {
    map: ImageMap,
    dirty: infra::DirtyCount,
    mat: infra::InternalCache<uint, Material>,
}
impl Image {
    pub fn image<'a>(&'a self) -> &'a ppapi::ImageData {
        &self.map.img
    }
    pub fn set_image(&mut self, img: ImageData) {
        self.map = img.map();
        self.dirty.bump()
    }
    fn render(&self, _ctxt: &mut Context3d) -> Option<Material> {
        use gl::material::ImageDataSource;
        self.mat
            .get(&self.dirty,
                 |last| {
                     let map = self.map.clone();
                     match last {
                         Some(mut last_mat) => {
                             last_mat.tex_units.as_mut_slice()[0].set_src(ImageDataSource(map));
                             last_mat
                         }
                         None => Material::new_with_tex_units
                             (vec!(TextureUnit::new_from_image_data(map))),
                     }
                 })
            .with(|mat_opt| mat_opt.map(|mat| mat.clone() ) )
    }
}

impl Animated<(), bool> for Image {
    fn animate(&mut self, _frame: &infra::Frame, _: ()) -> bool {
        self.mat.is_fresh(&self.dirty)
    }
}

pub type ChildrenMap = HashMap<Key, Element>;
pub struct Children {
    children:           HashMap<Key, Element>,
    children_dirty:     DirtyCount,

    rtt_size:           Size,
    // do we need to realloc the tex' storage?
    size_dirty:         Cell<bool>,
    // Does one of the children need a rendition? If not, we'll just use
    // a cached rendition.
    needs_rendition:    bool,

    tex:                Cell<Option<TextureBuffer>>,
    pub cascaded_input: Option<Key>,
}
struct ZIndexSorter<'a>(&'a Element, Material);
impl<'a> ZIndexSorter<'a> {
    fn element(&self) -> &'a Element {
        let &ZIndexSorter(inner, _) = self;
        inner
    }
    fn imm_material<'a>(&'a self) -> &'a Material {
        let &ZIndexSorter(_, ref mat) = self;
        mat
    }
    fn mut_material<'a>(&'a mut self) -> &'a mut Material {
        let &ZIndexSorter(_, ref mut mat) = self;
        mat
    }
}
impl<'a> cmp::Eq for ZIndexSorter<'a> { }
impl<'a, 'b> cmp::PartialEq for ZIndexSorter<'a> {
    fn eq(&self, rhs: &ZIndexSorter<'b>) -> bool {
        // Note this only checks that the element is the same
        let &ZIndexSorter(self_element, _) = self;
        let &ZIndexSorter(rhs_element, _)  = rhs;
        self_element as *const Element == rhs_element as *const Element
    }
}
impl<'a, 'b> cmp::PartialOrd for ZIndexSorter<'a> {
    fn partial_cmp(&self, rhs: &ZIndexSorter<'b>) -> Option<cmp::Ordering> {
        Some(self.cmp(rhs))
    }
}
impl<'a, 'b> cmp::Ord for ZIndexSorter<'a> {
    fn cmp(&self, rhs: &ZIndexSorter<'b>) -> cmp::Ordering {
        self.element().z_index.cmp(&rhs.element().z_index)
    }
}
impl Children {
    pub fn new(initial_size: Size, children: Option<HashMap<Key, Element>>) -> Content {
        ChildrenContent(Children {
            children:   children.unwrap_or_else(|| HashMap::new() ),
            children_dirty: Default::default(),
            rtt_size:   initial_size,
            size_dirty: Cell::new(true),
            tex:        Cell::new(Default::default()),

            needs_rendition: false,
            cascaded_input: None,
        })
    }
    // returns (the texture buffer, true) if we need to render the children.
    fn needs_rendition(&self, ctxt: &mut Context3d) -> (TextureBuffer, bool) {
        use ppapi::gles::consts::{TEXTURE_2D, UNSIGNED_BYTE};
        use ppapi::gles::RgbTexFormat;
        let mut new = false;
        let (tex, needs) = if self.tex.get().is_none() {
            let tex: TextureBuffer = GenBuffer::gen_single(ctxt);
            self.tex.set(Some(tex.clone()));
            new = true;
            (tex, true)
        } else {
            (self.tex.get().clone().unwrap(), self.needs_rendition)
        };

        if self.size_dirty.get() || new {
            let bound = tex.bind(ctxt, TEXTURE_2D);
            bound.image_2d(ctxt,
                           0, // mip lvl
                           RgbTexFormat, // internal format.
                           RgbTexFormat, // extern format.
                           self.rtt_size.clone(),
                           UNSIGNED_BYTE,
                           None);

            self.size_dirty.set(false);
            (tex, true)
        } else {
            (tex, needs)
        }
    }
    fn render(&self, ctxt: &mut ContextData) -> Option<Material> {
        use ppapi::gles::FrameBuffer;
        use ppapi::gles::traits::{BindableBuffer, GenBuffer};
        use ppapi::gles::consts::COLOR_ATTACHMENT0;
        use collections::priority_queue::PriorityQueue;
        use gl::enqueue_for_disposal;

        let (tex, needs) = self.needs_rendition(&mut ctxt.context);
        if needs {
            let mut queue: PriorityQueue<ZIndexSorter> =
                FromIterator::from_iter(self.children
                                        .values()
                                        .filter_map(|elem| {
                                            elem.render_content(ctxt)
                                                .map(|mat| ZIndexSorter(elem, mat.clone()) )
                                        }));
            
            let fbo: FrameBuffer = GenBuffer::gen_single(&ctxt.context);
            {   // bound_fbo region
                let mut bound_fbo = fbo.bind(&mut ctxt.context);
                bound_fbo.attach_tex2d(&ctxt.context,
                                       COLOR_ATTACHMENT0,
                                       tex,
                                       0);
                queue.maybe_pop()
                    .while_some(|ZIndexSorter(elem, mat)| {
                        elem.render(ctxt, &mat);
                        queue.maybe_pop()
                    });
            }
            enqueue_for_disposal(fbo);
        }
        Some(Material::new_with_tex_units(vec!(TextureUnit::new_from_tex_buf(tex))))
    }

    pub fn put_child(&mut self, key: Key, elem: Element) -> bool {
        self.children.insert(key, elem)
    }
    pub fn take_child(&mut self, key: Key) -> Option<Element> {
        self.children.pop(&key)
    }

    pub fn with_imm_child<Take, U>(&self,
                                   mut key: Vec<infra::Key>,
                                   take: Take,
                                   f: |&Element, Take| -> U) -> Option<U> {
        assert!(key.len() > 0);
        let this = key.shift().unwrap();
        match self.children.find(&this) {
            Some(elem) if key.len() == 0 => Some(f(elem, take)),
            Some(elem)                   => {
                match elem.imm_children() {
                    Some(c) => c.with_imm_child(key, take, f),
                    None => None,
                }
            }
            None                         => None,
        }
    }
    pub fn with_mut_child<Take, U>(&mut self,
                                   mut key: Vec<infra::Key>,
                                   take: Take,
                                   f: |&mut Element, Take| -> U) -> Option<U> {
        assert!(key.len() > 0);
        let this = key.shift().unwrap();
        match self.children.find_mut(&this) {
            Some(elem) => {
                if key.len() == 0 {
                    Some(f(elem, take))
                } else {
                    match elem.mut_children() {
                        Some(ref mut c) => c.with_mut_child(key, take, f),
                        None => None,
                    }
                }
            }
            None => None,
        }
    }
}
impl Animated<(), bool> for Children {
    fn animate(&mut self, frame: &infra::Frame, _: ()) -> bool {
        // if a sibling is animated, then animate will also be called on self.
        let Size {
            width: w,
            height: h,
        } = self.rtt_size.clone();
        let is_size_dirty = self.size_dirty.get();
        let subframe = frame.subframe();
        for (_, elem) in self.children.mut_iter() {
            elem.animate(&subframe, (w as f32, h as f32, is_size_dirty));
        }
        self.needs_rendition = subframe.rendition_requested() || is_size_dirty;
        self.needs_rendition
    }
}

#[unsafe_destructor]
impl Drop for Children {
    fn drop(&mut self) {
        use gl::enqueue_for_disposal;
        match self.tex.get() {
            Some(tex) => {
                enqueue_for_disposal(tex)
            }
            None => (),
        }
    }
}
// content is a 2d resource that is the surface of an element.
pub enum Content {
    TextContent(Text),
    ImgContent(Image),
    ChildrenContent(Children),
    NoContent,
}
impl Content {
    pub fn render(&self, ctxt: &mut ContextData) -> Option<Material> {
        match self {
            &TextContent(ref text) => text.render(&mut ctxt.context),
            &ImgContent(ref img) => img.render(&mut ctxt.context),
            &ChildrenContent(ref c) => c.render(ctxt),
            &NoContent => None,
        }
    }
    pub fn is_none(&self) -> bool {
        match self {
            &NoContent => true,
            _ => false,
        }
    }
}
impl Animated<(), bool> for Content {
    fn animate(&mut self, frame: &infra::Frame, _: ()) -> bool {
        match self {
            &TextContent(ref mut text) => text.animate(frame, ()),
            &ImgContent(ref mut img) => img.animate(frame, ()),
            &ChildrenContent(ref mut c) => c.animate(frame, ()),
            &NoContent => false,
        }
    }
}

impl Default for Content {
    fn default() -> Content {
        NoContent
    }
}

pub struct Element {
    pub pos: Animatible<math::Vec3>,
    pub orien: Animatible<math::Quaternion>,

    // 0 is the lowest priority.
    pub z_index: u16,

    content_dirty: DirtyCount,
    content: Content,
    mesh: gl::mesh::Handle,
    program: RefCell<shader::Program>,
}
// These are here for convenience. They'll likely be removed once I begin work
// on site viewer, which will be when a more robust system will be created.
static TEMP_VERTEX_SHADER: &'static str = "
#version 100
attribute vec3 a_position;
attribute vec3 a_normal;
attribute vec2 a_texcoord;

uniform mat4 u_projection;
uniform vec4 u_borders;

varying v_texcoord;

void main() {
    v_texcoord = a_texcoord;
    vec4 pos = vec4(clamp(a_position.x, u_borders.z, u_borders.x),
                    clamp(a_position.y, u_borders.w, u_borders.y),
                    a_position.z,
                    0.0);
    gl_Position = u_projection * pos;
}
";
static TEMP_FRAGMENT_SHADER: &'static str = "
#version 100
uniform sampler2D u_tex;

varying v_texcoord;

void main() {
    gl_FragColor = texture2D(u_tex, v_texcoord);
}
";

static BORDER_CLAMP_UNIFORM_KEY: Key = Key(10);
static BORDER_CLAMP_UNIFORM_ROLE: UniformRole = CustomUniformRole(BORDER_CLAMP_UNIFORM_KEY,
                                                                  Vec4Type(SingleFloatScalar));

pub fn build_shader_program(ctxt: &Context3d,
                            mgr: &mut shader::Manager__,
                            borders: Vec4) -> shader::Program {
    use gl::shader::{ProjMatrixUniformRole, SamplerUniformRole};
    use gl::mesh::TypedAttribute;
    use gl::Sampler2dKind;
    use math::vector::Vector;
    let attrs = vec!(("a_position".to_c_str(), TypedAttribute::default_position_attr()),
                     ("a_normal".to_c_str(),   TypedAttribute::default_normal_attr()),
                     ("a_texcoord".to_c_str(),   TypedAttribute::default_tex_coord_attr()));
    let uniforms = vec!(("u_projection".to_c_str(), ProjMatrixUniformRole),
                        ("u_tex".to_c_str(), SamplerUniformRole(Sampler2dKind, 0)),
                        ("u_border".to_c_str(), BORDER_CLAMP_UNIFORM_ROLE.clone()));
    match mgr.compile_simple(ctxt, TEMP_VERTEX_SHADER, TEMP_FRAGMENT_SHADER, &attrs, &uniforms) {
        Ok(mut prog) => {
            prog.set(&BORDER_CLAMP_UNIFORM_ROLE, FVec4Value(borders));
            prog
        }
        Err(cb) => fail!(cb(ctxt)),
    }
}

impl Element {
    pub fn new(mesh: gl::mesh::Handle, prog: shader::Program) -> Element {
        Element {
            pos:     Default::default(),
            orien:   Default::default(),

            content_dirty: Default::default(),
            content: Default::default(),
            z_index: 0,
            mesh:    mesh,
            program: RefCell::new(prog),
        }
    }
    pub fn set_content(&mut self, new: Content) {
        self.content_dirty.bump();
        self.content = new;
    }
    fn ensure_children_content<'a>(&'a mut self) -> &'a mut Children {
        if !self.has_children_content() {
            self.set_content(Children::new(Size::new(512, 512), None));
        }
        self.mut_children().unwrap()
    }
    pub fn has_children_content(&self) -> bool {
        match self.content {
            ChildrenContent(_) => true,
            _ => false,
        }
    }
    pub fn has_children(&self) -> bool {
        match self.content {
            ChildrenContent(ref c) if c.children.len() != 0 => true,
            _ => false,
        }
    }
    pub fn drop_children(&mut self) {
        if self.has_children_content() {
            self.content = NoContent;
        }
    }

    pub fn imm_children<'a>(&'a self) -> Option<&'a Children> {
        match self.content {
            ChildrenContent(ref c) => Some(c),
            _ => None,
        }
    }
    pub fn mut_children<'a>(&'a mut self) -> Option<&'a mut Children> {
        match self.content {
            ChildrenContent(ref mut c) => Some(c),
            _ => None,
        }
    }

    pub fn with_imm_children<U>(&self, f: |&Children| -> U) -> Option<U> {
        self.imm_children().map(|c| f(c) )
    }
    pub fn with_mut_children<U>(&mut self, f: |&mut Children| -> U) -> Option<U> {
        self.mut_children().map(|c| f(c) )
    }

    pub fn put_child(&mut self, key: Key, elem: Element) -> bool {
        self.ensure_children_content().children.insert(key, elem)
    }
    pub fn take_child(&mut self, key: Key) -> Option<Element> {
        self.mut_children().and_then(|c| c.children.pop(&key) )
    }

    fn render_content(&self, ctxt: &mut ContextData) -> Option<Material> {
        self.content.render(ctxt)
    }
    pub fn render(&self, ctxt: &mut ContextData, mat: &Material) {
        mat.update_binding_values(&mut *self.program.borrow_mut());
        mat.activate(&mut ctxt.context);
        self.program.borrow().bind(&mut ctxt.context);
        ctxt.mesh.render(&mut ctxt.context, &self.mesh);
    }

    /*fn child_region_filter_iter<T: Iterator<ExactRegion<f32>>>(&self) -> T {
        self.children
            .values()
            .filter_map(|&element| {
                let (w, h) = element.region();
                let is_w_zero = match w {
                    ExactRelRegion(0.0) | ExactStaticRegion(0.0) => true,
                    _ => false,
                };
                let is_h_zero = match h {
                    ExactRelRegion(0.0) | ExactStaticRegion(0.0) => true,
                    _ => false,
                };
                if is_w_zero || is_h_zero { None }
                else {
                    Some((match w {
                        ExactRelRegion(w) => ExactRelRegion(w + element.pos[0]),
                        ExactStaticRegion(w) => ExactStaticRegion(w),
                    }, match h {
                        ExactRelRegion(h) => ExactRelRegion(h + element.pos[1]),
                        ExactStaticRegion(h) => ExactStaticRegion(h),
                    }))
                }
            })
    } 
    fn possibly_bind(&self, region: AxisRegion<f32>) -> ExactRegion<f32> {
        match region {
            BoundedRegion(min, max) => ExactRegion::bind(min,
                                                         max,
                                                         self.child_region_filter_iter()),
            RelRegion(v) =>        ExactRelRegion(v),
            StaticRegion(v) =>     ExactStaticRegion(v),
        }
    }
    pub fn region(&self) -> (ExactRegion<f32>, ExactRegion<f32>) {
        let width = self.possibly_bind(self.region.width);
        let height = self.possibly_bind(self.region.height);
        (width, height)
    }*/
}

impl Animated<(f32, f32, bool), ()> for Element {
    fn animate(&mut self, frame: &infra::Frame,
               (_parent_width, _parent_height, _parent_resized): (f32, f32, bool)) {
        let request = self.pos.animate(frame, ());
        let request = self.orien.animate(frame, ()) || request;
        if self.content.animate(frame, ()) || request {
            frame.request_rendition();
        }
    }
}
